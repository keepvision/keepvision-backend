var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var User = new Schema({
  _id: {
    type: String
  },
  emails: [
    {
      address:  String
    }
  ],
  services:{
    password:{
      bcrypt:String
    }
  },
  api_key:String
});

module.exports.User = mongoose.model('Users', User, 'users');