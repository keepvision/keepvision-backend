var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var IntervalMatrix = new Schema({
  age_from:Number,
  age_to:Number,
  interval_type:Number,
  interval_yellow:Number,
  interval_red:Number
});

module.exports.IntervalMatrix = mongoose.model('IntervalMatrix', IntervalMatrix, 'interval_matrix');