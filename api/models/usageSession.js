var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var UsageSession = new Schema({
  child:{
    id:String
  },
  date_begin:Date,
  date_end:Date,
  date_yellow:Date,
  date_red:Date,
  duration:Number,
  interval:Number,
  log:{ type : Array , "default" : [] },
  message_is_send:Number,
  lighting_level:Number,
  lighting_count:Number
});

module.exports.UsageSession = mongoose.model('UsageSession', UsageSession, 'usage_session');