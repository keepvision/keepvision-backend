var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var FactorMatrix = new Schema({
  name:String,
  code:Number,
  age_from:Number,
  age_to:Number,
  value_from:Number,
  value_to:Number,
  factor:Number
});

module.exports.FactorMatrix = mongoose.model('FactorMatrix', FactorMatrix, 'factor_matrix');