var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var Diagnosis = new Schema({
 child:{
 	id:String
 },
 date_update:Date,
 diagnosis_data:{
 	date_of_test:Date,
        lighting_level:Number,
 	acuity:Number,
 	distance:Number,
        back_bright: Number,  
        score: Number,
 	tests:[
 		{
 	"serial_number": Number,	
        "ring_size": Number,	
        "ring_color": String,	
        "ring_velocity": Number,	
        "ring_vector": Number,	
        "swipe_time": Number,	
        "swipe_error": Number,	
        "distance_error": Number,	
        "back_color": String,		
        "acuity": Number,
        "state":Number
 		}
 	]
 },
 vision_safety:Number
});

module.exports.Diagnosis = mongoose.model('Diagnosis', Diagnosis, 'diagnosis');