var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var Child = new Schema({
 _id:String,
 name:String,
 user:{
 	id:String
 },
 gender:Number,
 birthday:Date,
 date_update:Date,
 last_usage_session_id:String,
 options:{
 	sharpness:Number,
 	parents_desease_vision:Number
 }
});

module.exports.Child = mongoose.model('Child', Child, 'child');