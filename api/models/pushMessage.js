var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var PushMessage = new Schema({
	user:{
  	id:String
  },
  title:String,
  text:String,
  status:Number,
  childId:String,
  date:Date
});

module.exports.PushMessage = mongoose.model('PushMessage', PushMessage, 'push_message');