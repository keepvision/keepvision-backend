var constants = require('../libs/constants');
textMessage = [];
textMessage[constants.MESSAGE_IS_SEND_SAFE] = {
	title:"safe",
	text:"safe"
};
textMessage[constants.MESSAGE_IS_SEND_YELLOW] = {
	title:"Yellow zone",
	text:"Yellow zone"
};
textMessage[constants.MESSAGE_IS_SEND_RED] = {
	title:"Red zone",
	text:"Red zone"
};
module.exports = textMessage;