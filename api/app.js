var express = require('express');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var path = require('path');

var config = require('./config/config');
var log = require('./libs/log')(module);

var cors = require('cors');

var app = express();

app.use(cors());

app.use(methodOverride('X-HTTP-Method-Override'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

var route = require('./routes/routes')(app);

app.use(express.static(path.join(__dirname, '../frontend')));

app.use(function(req, res, next){
  log.debug('Not found URL: %s',req.url);
  return res.status(404).send({ error: 'Not found' });
});

app.use(function(err, req, res, next){
  log.error('Internal error(%d): %s',res.statusCode,err.message);
  return res.status(err.status || 500).send({ error: err.message });
});

app.listen(config.get('port'), function(){
  log.info('Express server listening on port ' + config.get('port'));
});