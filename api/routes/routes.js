var mongoose = require('../libs/mongoose').mongoose;

module.exports = function(app) {
  app.get('/api/v1/actions/get_api_key', require('./getApiKey').get);
  app.get('/api/v1/users/:user_id/children', require('./childrenList').get);
  app.post('/api/v1/children/:child_id/events', require('./childEvents').post);
};
