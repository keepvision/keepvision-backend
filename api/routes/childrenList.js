"use strict";

var url = require('url');
var qs = require('querystring');
var bcrypt = require('bcrypt-nodejs');
var crypto = require('crypto');
var UserModel = require('../models/user').User;
var ChildModel = require('../models/child').Child;
var log = require('../libs/log')(module);

// Запрос списка детей
exports.get = function(req, res) {
  var REST_NAME = '/api/v1/users/{user_id}/children: ';

  var query = url.parse(req.url).query,
  parsed = qs.parse(query),
  security_token = parsed.security_token,
  userId = req.params.user_id;

  if (!security_token) {
    log.error(REST_NAME + 'Incorrect arguments: security_token required');
    return res.status(400).send('Incorrect arguments: security_token required');
  }

  UserModel.findById(userId, function(err, user) {
  	if (!user) {
  		log.error(REST_NAME + 'Authorization failed: user not found:' + userId);
      return res.status(403).send('Authorization failed: user not found');
    }
    if (err) {
      log.error(REST_NAME + 'Server failed:' + err);
      return res.status(501).send('Server failed');
    }

    if (crypto.createHash('sha256').update(userId + user.api_key).digest('hex') !== security_token) {
     log.error(REST_NAME + 'Authorization failed: incorrect security_token');
     return res.status(403).send('Authorization failed: incorrect security_token');
   }

   ChildModel.find({'user.id':userId},'_id name', function(err, children) {
    if (!children) {
      log.error(REST_NAME + 'Child not found');
      return res.status(404).send('Child not found');
    }
    if (err) {
      log.error(REST_NAME + 'Server failed:' + err);
      return res.status(501).send('Server failed');
    }

    return res.status(200).send(children);
  });
 });
};