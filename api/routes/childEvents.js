"use strict";

var url = require('url');
var qs = require('querystring');
var bcrypt = require('bcrypt-nodejs');
var crypto = require('crypto');
var UserModel = require('../models/user').User;
var ChildModel = require('../models/child').Child;
var UsageSession = require('../models/usageSession').UsageSession;
var Diagnosis = require('../models/diagnosis').Diagnosis;
var IntervalMatrix = require('../models/intervalMatrix').IntervalMatrix;
var FactorMatrix = require('../models/factorMatrix').FactorMatrix;
var log = require('../libs/log')(module);
var REST_NAME;
var textMessage = require('../libs/textMessage');
var constants = require('../libs/constants');

// Сообщение о событии в приложении ребёнка
exports.post = function(req, res) {
  var query = url.parse(req.url).query,
  parsed = qs.parse(query),
  security_token = parsed.security_token,
  childId = req.params.child_id,
  eventCode = req.body.event_code,
  eventOptions = req.body.event_options;
  REST_NAME = new Date + '/api/v1/children/' + childId + '/events: ';
  if (!security_token) {
    log.error(REST_NAME + 'Incorrect arguments: security_token required');
    return res.status(400).send('Incorrect arguments: security_token required');
  }
  if (typeof eventCode !== "number") {
    log.error(REST_NAME + 'Incorrect arguments: event_code required');
    return res.status(400).send('Incorrect arguments: event_code required');
  }

  ChildModel.findById(childId, function(err, child) {
  	if (!child) {
  		log.error(REST_NAME + 'Child not found:' + childId);
      return res.status(404).send('Child not found');
    }
    if (err) {
      log.error(REST_NAME + 'Server failed:' + err);
      return res.status(501).send('Server failed');
    }

    var userId = child.user.id;
    UserModel.findById(userId, 'api_key', function(err, user) {
      if (!user) {
        log.error(REST_NAME + 'User not found');
        return res.status(501).send('User not found');
      }
      if (err) {
        log.error(REST_NAME + 'Server failed:' + err);
        return res.status(501).send('Server failed');
      }
      
      if (!checkHash(childId, eventCode, eventOptions, user.api_key, security_token)) {
        log.error(REST_NAME + 'Authorization failed: incorrect security_token');
        return res.status(403).send('Authorization failed: incorrect security_token');
      }

      switch(eventCode) {
        case constants.EVENT_CODE_TIME_TRACKING: 
          tickTimeTracking(child, userId, eventOptions, res);
          break;
        case constants.EVENT_CODE_DIAGNOSIS:
          saveDiagnosis(eventOptions, childId, res);
          break;
        default:
          log.error(REST_NAME + 'Incorrect arguments: event_code');
           return res.status(400).send('Incorrect arguments: event_code');
          break;
      }
    });
  });
}

/**
 * checkHash() Подсчетывает Хэш для аргументов и сравнивает его с токеном.
 *  При совпадении возвращает истину, иначе ложь.
 *
 * @param <String> childId
 * @param <Number> eventCode
 * @param <Object> eventOptions
 * @param <String> api_key
 * @param <String> security_token
 */
function checkHash(childId, eventCode, eventOptions, api_key, security_token) {
      var paramStr = childId + eventCode;
      for(var p in eventOptions) {
        if (p === 'tests') {
           eventOptions[p].forEach(function(elem) {
            for(var key in elem) {
              if (typeof elem[key] !== 'undefined' ) paramStr += elem[key];
            }
           });
        }
        else if (typeof eventOptions[p] !== 'undefined' ) paramStr += eventOptions[p];
      }
      paramStr += api_key;
      console.log(paramStr);
      if (crypto.createHash('sha256').update(paramStr).digest('hex') === security_token) {
        return true;
      } 
      return false;
}

/**
 * tickTimeTracking() изменяет значение в коллекции usage_session
 * для трекинга времени
 *
 * @param <Object> child
 * @param <String> userId
 * @param <Object> eventOptions
 * @param <Object> res
 */
 function tickTimeTracking(child, userId, eventOptions, res) {
 
  if (!child.birthday) {
    log.error(REST_NAME + 'birthday is undefined');
    return res.status(501).send('Server failed');
  }

   if (!(eventOptions && eventOptions.start)) {
    log.error(REST_NAME + 'event_options.start is undefined');
    return res.status(400).send('event_options.start required');
  }

  var ms = Date.parse(eventOptions.start);
  if (isNaN(ms)) {
    log.error(REST_NAME + 'event_options.start is incorrect');
    return res.status(400).send('event_options.start is incorrect');
  }

  if (child.last_usage_session_id) {
    UsageSession.findById(child.last_usage_session_id, function(err, lastSession) {
      if (err) {
        log.error(err);
        return res.status(501).send('Server failed');
      }
      if (!lastSession) {
        log.error(REST_NAME + 'usage_session with id:' + child.last_usage_session_id + ' not found');
        return res.status(501).send('Server failed');
      }
     
      if (lastSession.date_begin.getTime() ===  getDateBegin(eventOptions).getTime()) { 
        //Продолжаем текущую сессию
        findInterval(child, lastSession, userId, eventOptions, res);
      } else { 
        UsageSession.find({date_begin:eventOptions.start, "child.id":child._id}, function(err, session) {
          if (session.length !== 0) {
            //Обновляем найденную сессию
            findInterval(child, session[0], userId, eventOptions, res);
          }
          else {
            // Начинаем новую сессию
            createNewSessionUsage(res, child._id, eventOptions);
          }
        });
      }

    });
  } else {
    //Первая сессия у ребенка
    createNewSessionUsage(res, child._id, eventOptions ); 
  }
}

/**
 * createNewSessionUsage() Создать новую сессию
 * 
 * @param <Object> res
 * @param <String> childId
 * @param <Object> eventOptions 
 */
 function createNewSessionUsage(res, childId, eventOptions ) {
   var dataSession = {},
   continue_using = eventOptions.continue_using?+eventOptions.continue_using:0,
   interval = eventOptions.relax?eventOptions.relax:0;

   dataSession.child = {};
   dataSession.child.id = childId;
   dataSession.date_begin = getDateBegin(eventOptions);
   dataSession.date_end = getDateEnd(eventOptions);
   dataSession.duration = fromMsInMinute(continue_using);
   dataSession.message_is_send = constants.MESSAGE_IS_SEND_SAFE;
   dataSession.interval = fromMsInMinute(interval);
   if ( typeof eventOptions.lighting_level !== "undefined" && +eventOptions.lighting_level !== -1) {
     dataSession.lighting_level = +eventOptions.lighting_level;
     dataSession.lighting_count = 1;
   }
   if (!dataSession.log) dataSession.log = [];
   dataSession.log.push(eventOptions);

   var usageSession = new UsageSession(dataSession);
   usageSession.save(function(err, doc) {
    if (err) {
      log.error(REST_NAME + err);
      return res.status(501).send('Server failed');
    }
    ChildModel.findByIdAndUpdate(childId, {last_usage_session_id:doc._id}, function(err) {
      if (err) log.error(REST_NAME + err);
      return res.status(201).send('OK');
    });
  });
}

/**
 * saveDiagnosis() Сохранить диагноз 
 * 
 * @param <Object> eventOptions
 * @param <Object> childId
 * @param <Object> res
 */
function  saveDiagnosis(eventOptions, childId, res) {
    var diagnosisData = {};
    for(var p in eventOptions) {
        if (typeof eventOptions[p] !== 'undefined') diagnosisData[p] = eventOptions[p];
    }
    var data = {};
    data.child = {};
    data.child.id = childId;
    if (eventOptions.date_of_test) data.date_update = new Date(eventOptions.date_of_test);
    else data.date_update = new Date();
    data.diagnosis_data = diagnosisData;
    data.vision_safety = calculateVisionSafety(diagnosisData.acuity);

    var diagnosis =  new Diagnosis(data);
    diagnosis.save(function(err){
      if (err) {
        log.error(REST_NAME + err);
        return res.status(501).send('Server failed');
      }
      return res.status(201).send('OK');
    });
}

/**
 * calculateVisionSafety() Рассчитать сохранность зрения
 * 
 * @param <Number> acuity острота зрения
 */
function calculateVisionSafety(acuity) {
  if (!acuity) return;
  var table = constants.VISUAL_ACUITY_TABLE,
  currLine;
  for(var i = 0; i < table.length; i++) {
    if ( acuity <= table[i].acuity) {
      currLine = i;
      break;
    }
  }
  if (typeof currLine === 'undefined' ) return;

  
  if (currLine === 0 ) { 
    return table[currLine];
  }
  else if ( currLine < table.length) 
  {
    var prevLine = currLine - 1,
    a = table[currLine].vision - table[prevLine].vision,
    b = table[currLine].acuity - table[prevLine].acuity,
    c = acuity - table[prevLine].acuity;
    if (b !== 0) return a * c / b + table[prevLine].vision;
  }
}


/**
 * findInterval() Найти интервал в матрице интервалов
 * 
 * @param <Object> child ребенок
 * @param <Object> currentSession текущая(обновляемая) сессия
 * @param <String> userId id пользователя
 * @param <Object> eventOptions
 * @param <Object> res
 */
function findInterval(child, currentSession, userId, eventOptions, res) { 
  if (!child.birthday) {
    log.error(REST_NAME + 'birthday is undefined');
    return res.status(501).send('Server failed');
  }

  var age = getAgeChild(child.birthday),
  dateBeginSession = getDateBegin(eventOptions);
  IntervalMatrix.findOne({age_from:{$lte:age}, age_to:{$gte:age}}, function(err, interval) {
    if (err) {
      log.error(REST_NAME + err);
      return res.status(501).send('Server failed');
    }
    if (!interval) {
      log.error(REST_NAME + 'interval not found');
      return res.status(501).send('Server failed');
    }
    var dateBeginPrevDay = prevDay(dateBeginSession);
    
    //Пока без учета перехода сессии с одного дня на другой
    UsageSession.find({date_begin:{ 
      "$gte": dateBeginPrevDay,
      "$lt": dateBeginSession
    },"child.id":child._id}, function(err, session) {
      var durationUseDay = fromMsInMinute(+eventOptions.continue_using);
      session.forEach(function(oneSession) { 
        durationUseDay += oneSession.duration;
      }); 
      calculateCoefficient(currentSession, age, child, durationUseDay, interval, userId, eventOptions, res);  
    });
  });
}

/**
 * calculateCoefficient() Найти коэффициент в матрице коэффициентов
 * 
 * @param <Object> currentSession Обновляемая сессия
 * @param <Number> age Возраст ребенка
 * @param <Object> child ребенок
 * @param <Number> durationUseDay Длительность использования за сутки
 * @param <Object> interval Интервал из матрицы интервалов
 * @param <String> userId id пользователя
 * @param <Object> eventOptions
 * @param <Object> res
 */
function calculateCoefficient(currentSession, age, child, durationUseDay, interval, userId, eventOptions,  res) {
  FactorMatrix.find({}, function(err, docs) {
    if (err) {
      log.error(REST_NAME + err);
      return res.status(501).send('Server failed');
    }

    var durationBreaksFactor = findFactorInMatrix(docs, constants.FACTOR_MATRIX_DURATION_BREAKS_CODE, age, currentSession.interval),
    durationUseFactor = findFactorInMatrix(docs, constants.FACTOR_MATRIX_DURATION_USE_CODE, age, durationUseDay);
    if (child.options && child.options.sharpness) {
      var sharpnessFactor = findFactorInMatrix(docs, constants.FACTOR_MATRIX_SHARPNESS_VISION_CODE, age, child.options.sharpness);
    }
    else var sharpnessFactor = 1
    
    var genderFactor = findFactorInMatrix(docs, constants.FACTOR_MATRIX_GENDER_CODE, age, child.gender);
    if (child.options && child.options.parents_desease_vision) {
      var heredityFactor = findFactorInMatrix(docs, constants.FACTOR_MATRIX_HEREDITY_CODE, age, child.options.parents_desease_vision);
    }
    else var heredityFactor = 1;

    if (currentSession.lighting_level && currentSession.lighting_count) {
      var lighting = currentSession.lighting_level / currentSession.lighting_count;
      var lightingFactor = findFactorInMatrix(docs, constants.FACTOR_MATRIX_LIGHTING_CODE, age, child.gender);
    }
    else var lightingFactor = 1;
   
    
    var coefficient =  durationBreaksFactor * durationUseFactor * sharpnessFactor * genderFactor * heredityFactor * lightingFactor; 
    updateSession(interval, coefficient, eventOptions, currentSession, userId, child, res);
  });
}

/**
 * updateSession() обновить сессию и отправить сообщение
 * 
 * @param <Object> interval Интервал из матрицы интервалов
 * @param <Number> coefficient  коэффициент рассчитанный на основе матрицы коэффициентов
 * @param <Object> eventOptions
 * @param <Object> currentSession Обновляемая сессия
 * @param <String> userId id пользователя
 * @param <Object> child данные о ребенке
 * @param <Object> res
 */
function updateSession(interval, coefficient, eventOptions, currentSession, userId, child, res ) {
  var duration = fromMsInMinute(+eventOptions.continue_using),
  relax = eventOptions.relax?eventOptions.relax:0,
  messageIsSend;

  if (interval.interval_red * coefficient < duration)  messageIsSend = constants.MESSAGE_IS_SEND_RED;
  else if (interval.interval_yellow * coefficient < duration ) messageIsSend = constants.MESSAGE_IS_SEND_YELLOW;
  else messageIsSend = constants.MESSAGE_IS_SEND_SAFE;

  var dataUpdate = {
    date_end: getDateEnd(eventOptions), 
    duration:duration
  };

  if ( typeof eventOptions.lighting_level !== "undefined" && +eventOptions.lighting_level !== -1) {
     if (currentSession.lighting_level) {
      dataUpdate.lighting_level = +currentSession.lighting_level + +eventOptions.lighting_level;
     }
     else  dataUpdate.lighting_level = eventOptions.lighting_level;

     if (currentSession.lighting_count) {
      dataUpdate.lighting_count = currentSession.lighting_count + 1;
     }
     else  dataUpdate.lighting_count = 1;
   }

  if (relax) dataUpdate.interval = fromMsInMinute(relax);

  if (messageIsSend !== currentSession.message_is_send) {
    dataUpdate.message_is_send = messageIsSend;
    createPushMessage(userId, textMessage[messageIsSend].title, 
                              textMessage[messageIsSend].text,
                              child._id,
                              dataUpdate.date_end);
    if (messageIsSend === constants.MESSAGE_IS_SEND_RED) dataUpdate.date_red = dataUpdate.date_end;
    if (messageIsSend === constants.MESSAGE_IS_SEND_YELLOW) dataUpdate.date_yellow = dataUpdate.date_end;
  }
  if (!currentSession.log) dataUpdate.log = [];
  else dataUpdate.log = currentSession.log;
  
  dataUpdate.log.push(eventOptions);
  if (currentSession.date_end < dataUpdate.date_end) {
    UsageSession.findByIdAndUpdate(currentSession._id, dataUpdate, function(err) {
      if (err) {
        log.error(err);
        return res.status(501).send('Server failed');
      }
      return res.status(201).send('OK');
    });
  }
  else  return res.status(201).send('OK');
}


/**
 * getAgeChild() Возвращает возраст ребенка на текущий момент
 * 
 * @param <Date> birthday День рождения ребенка
 */
function getAgeChild(birthday) {
  var ageDifMs = Date.now() - birthday,
  ageDate = new Date(ageDifMs);
  ageDate.getUTCFullYear();
  return Math.abs(ageDate.getUTCFullYear() - 1970);
}

/**
 * createPushMessage() Создать Push-уведомление в коллекции push_message
 * 
 * @param <String> userId Id пользователя
 * @param <String> title Заголовок уведомления 
 * @param <String> text Текст уведомления
 * @param <String> ChildId Id ребенка
 * @param <Object> date дата отчета
 */
function createPushMessage(userId, title, text, childId, date) { 
  var PushMessage = require('../models/pushMessage').PushMessage;
  var message = new PushMessage({
    user:{
      id:userId
    },
    title:title,
    text:text,
    status:constants.PUSH_MESSAGE_CREATE,
    childId: childId,
    date:date
  });
  message.save(function(err) {
    if (err)  log.error(REST_NAME + err);
  });
}

/**
 * fromMsInMinute() Преобразование из миллисекунд в минуты
 * 
 * @param <Number> ms Время в миллисекунд
 * @return <Number> время в минутах
 */
function fromMsInMinute(ms) {
  if (!ms) return 0;
  var MS_IN_MINUTE = 60000;
  return Math.round(ms / MS_IN_MINUTE);
}


/**
 * findFactorInMatrix() поиск коэффициента в матрице коэффициентов
 * 
 * @param <Object> matrix матрица коэффициентов
 * @param <Number> code код коэффициента
 * @param <Number> age Возраст ребенка
 * @param <Number> value Значени аргумента
 * @return <Number> время в минутах
 */
function findFactorInMatrix(matrix, code, age, value ) {
  var factor = 1;
  matrix.forEach(function(line) {
    if ((line.code === code) &&
       (line.age_from <= age) && ( age <= line.age_to) &&
       (line.value_from <= value) && ( value <= line.value_to)) {
      factor = line.factor / 100;
    }
  });
  return factor;
}


/**
 * prevDay() Получить предыдущий день
 * 
 * @param <Date> date 
 * @return <Date> предыдущий день
 */
function prevDay(date) {
  if (!date) return;
  var day = date.getDate(),
  yesterday = new Date(date);
  yesterday.setDate( day - 1 );
  return yesterday;
}


function getDateBegin(eventOptions) {
  return new Date(eventOptions.start);
}

function getDateEnd(eventOptions) {
  var  continue_using = eventOptions.continue_using?+eventOptions.continue_using:0;
  return new Date(getDateBegin(eventOptions).getTime() + continue_using);
}