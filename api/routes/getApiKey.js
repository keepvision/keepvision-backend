"use strict";

var url = require('url');
var qs = require('querystring');
var bcrypt = require('bcrypt-nodejs');
var crypto = require('crypto');
var UserModel = require('../models/user').User;
var log = require('../libs/log')(module);

// Получение API_key
exports.get = function(req, res) {
  var REST_NAME = '/api/v1/actions/get_api_key: ';
 
  var query = url.parse(req.url).query,
  parsed = qs.parse(query),
 	email = parsed.email,
 	password = parsed.password;

  if (!email) {
    log.error(REST_NAME + 'Incorrect arguments: email is not valid');
    return res.status(400).send('Incorrect arguments: email is not valid');
  }
  if (!password) {
    log.error(REST_NAME + 'Incorrect arguments: password required');
    return res.status(400).send('Incorrect arguments: password required');
  }

  UserModel.findOne({'emails.address':email}, function(err, user) {
  	if (!user) {
  		log.error(REST_NAME + 'Authorization failed: email not found:' + email);
      return res.status(403).send('Authorization failed: email not found');
  	}
    if (err) {
      log.error(REST_NAME + 'Server failed:' + err);
      return res.status(501).send('Server failed');
    }

    var passwordSha256 = crypto.createHash('sha256').update(password).digest('hex');
    if (!bcrypt.compareSync(passwordSha256, user.services.password.bcrypt)) {
      log.error(REST_NAME + 'Authorization failed: incorrect password:' + password);
      return res.status(403).send('Authorization failed: incorrect password');
    }
    return res.status(200).send({
    	"user_id":user._id,
      "api_key":user.api_key
    });
  });
};