SimpleSchema.messages({
	required:'[label] необходимо заполнить',
	minNumber:'[label] должен быть минимум [min]',
	minString:'[label] должен быть минимум [min] символов',
	regEx: [
		{exp: SimpleSchema.RegEx.Email, msg: "должен быть правильным e-mail адресом"}
	],
	'passwordMismatch':'пароли не совпадают',
	'ageValue': 'конечный возраст должен быть больше начального',
	'redYellow': 'время красной зоны должно быть больше желтой',
	'value': 'начальное значение должно быть меньше конечного',
	notMoney:'Не достаточно денег на балансе'
});