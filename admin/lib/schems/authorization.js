this.Schems = this.Schems || {};
Schems.authorization = new SimpleSchema({
	email:{
		type:String,
		regEx: SimpleSchema.RegEx.Email,
    label:'email'
	},
	password:{
		type:String,
		label:'пароль',
		min:3
	}
});

Schems.forgotPassword = new SimpleSchema({
	email:{
		type:String,
		regEx: SimpleSchema.RegEx.Email,
    label:'email'
	}
});