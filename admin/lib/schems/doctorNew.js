this.Schems = this.Schems || {};
Schems.doctorNew = new SimpleSchema({
    first_name: {
        type: String,
        label: 'first name',
        max: 30
    },
    last_name: {
        type: String,
        label: 'last name',
        max: 30
    },
    email: {
        type: String,
        regEx: SimpleSchema.RegEx.Email,
        label: 'email'
    },
	address: {
        type: String,
        label: 'address',
		optional: true,
        max: 30
    },
	latitude: {
        type: Number,
        label: 'latitude',
		optional: true,
        max: 90,
		min: 0,
		decimal: true
    },
	longitude: {
        type: Number,
        label: 'longitude',
		optional: true,
        max: 90,
		min: 0,
		decimal: true
    },
    phone: {
        type: String,
        label: 'phone',
		optional: true,
        max: 30
    },
    website: {
        type: String,
        label: 'website',
		optional: true,
        max: 30
    },
	type_doctor: {
        type: Number,
        label: 'type doctor',
        max: 1,
		optional: true,
		min: 0
    }
});
