this.Schems = this.Schems || {};
Schems.adminNew = new SimpleSchema({
	email: {
		type: String,
		regEx: SimpleSchema.RegEx.Email,
		label: 'email'
	},
	first_name: {
		type: String,
		label: 'first_name',
		max: 30
	},
	last_name: {
		type: String,
		label: 'last_name',
		max: 30
	}
});
