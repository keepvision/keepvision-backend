Router.configure({
	layoutTemplate: 'layout',
	loadingTemplate: 'loading'
});

Router.route('mainPage', {
	path: '/'
});

Router.route('listUsers', {
	path: '/users',
	waitOn: function() {
		Session.setDefault('skipUser', 0);
		Session.setDefault('less', true);
		Session.setDefault('more', false);
		Session.setDefault('searchName', false);
		Session.setDefault('search', false);
		
		Session.set('notFound', false);
		Session.set('resultOfSearch', false);
		
		var skip = Session.get('skipUser');
		if (skip > -1) {
			return [
				Meteor.subscribe('usersAll', CONSTANTS.SKIP_USERS, skip)
			]
		} 
	}
});

Router.route('pageUser', {
	path: '/users/:_id',
	waitOn: function() {
		Session.setDefault('skipUser', 0);
		Session.setDefault('less', true);
		Session.setDefault('more', false);
		Session.setDefault('searchName', false);
		Session.setDefault('search', false);
		
		Session.set('notFound', false);
		Session.set('resultOfSearch', false);
		
		var skip = Session.get('skipUser');
		if (skip > -1) {
			return [
				Meteor.subscribe('usersAll', CONSTANTS.SKIP_USERS, skip),
				Meteor.subscribe('userOne', this.params._id),
				Meteor.subscribe('children', this.params._id)
			]
		}
	},
	data: function() {
	/*	Session.set('userStatusRemove', false);
		Session.set('userStatusActive', false);
		
		var t = Meteor.users.find({_id: this.params._id}).fetch()[0];
		if (t && t.status == CONSTANTS.USER_STATUS_REMOVE) Session.set('userStatusRemove', true);
		if (t && t.status == CONSTANTS.USER_STATUS_ACTIVE) Session.set('userStatusActive', true);
		*/
		return {
			userOne: Meteor.users.find({_id: this.params._id}).fetch()[0],
			child: Children.find({user: {id: this.params._id}})
		};
	}
});

Router.route('pageChild', {
	path: '/children/:_id',
	waitOn: function() {		
		Session.setDefault('skipSession', 0);
		Session.setDefault('lessSession', true);
		Session.setDefault('moreSession', false);
		
		Session.setDefault('skipDiagnosis', 0);
		Session.setDefault('lessDiagnosis', true);
		Session.setDefault('moreDiagnosis', false);
		
		Session.set('sessionMore', true);
		
		//Session.set('diagnosis', false);
		//Session.set('session', false);
		
		var t = Children.find({_id: this.params._id}).fetch()[0];
		var skip = Session.get('skipSession');
		var skip2 = Session.get('skipDiagnosis');
		return [
			Meteor.subscribe('child', this.params._id),
			Meteor.subscribe('diagnosis', this.params._id, CONSTANTS.SKIP_SESSIONS, skip2),
			Meteor.subscribe('session', this.params._id, CONSTANTS.SKIP_SESSIONS, skip),
			Meteor.subscribe('userOne', t && t.user.id),
			//Meteor.subscribe('usersAll', 100, 0),
			//Meteor.subscribe('children', t && t.user.id)
			Meteor.subscribe('childrenAll')
		]
	},
	data: function() {		
		Session.set('genderFemale', false);
		Session.set('genderMale', false);
		Session.set('goodParrent', false);
		Session.set('badParrent', false);
		
		var t = Children.find({_id: this.params._id}).fetch()[0];
		if (t && t.gender == CONSTANTS.CHILD_GENDER_FEMALE) Session.set('genderFemale', true);
		if (t && t.gender == CONSTANTS.CHILD_GENDER_MALE) Session.set('genderMale', true);
		
		if (t && t.options) {
			if (t && t.options.parents_desease_vision == CONSTANTS.CHILD_PARENTS_DESEASE_FALSE) Session.set('goodParrent', true);
			if (t && t.options.parents_desease_vision == CONSTANTS.CHILD_PARENTS_DESEASE_TRUE) Session.set('badParrent', true);
		}
		
		return {
			child: Children.find({_id: this.params._id}).fetch()[0],
			diagnosis: Diagnosis.find({child: {id: this.params._id}}),
			session: Ses.find({child: {id: this.params._id}}),
			parrent: Meteor.users.find({_id: t && t.user.id}).fetch()[0],
			children: Children.find({user: {id: t && t.user.id}}).fetch()
		};
	}
});

Router.route('listAdmins', {
	path: '/admins',
	waitOn: function() {
		//Session.set('addAdmin', false);

        return Meteor.subscribe('adminsAll');
    }
});

Router.route('pageAdmin', {
    path: '/admins/:_id',
    waitOn: function() {
		return [
			Meteor.subscribe('adminsAll'),
			Meteor.subscribe('adminOne', this.params._id)
		]
    },
    data: function() {
        return {
            adminId: this.params._id
        };
    }
});

Router.route('newAdmin', {
    path: '/admin-new',
	waitOn: function() {
		Session.set('letterSend', false);
		
        return Meteor.subscribe('adminsAll');
    }
});

Router.route('listDoctors', {
	path: '/doctors',
    waitOn: function() {
        Session.setDefault('skipDoctor', 0);
        Session.setDefault('less', true);
        Session.setDefault('more', false);
        Session.setDefault('searchName', false);

        Session.set('notFound', false);
        Session.set('resultOfSearch', false);
		
	//	Session.set('doctor', false);
		//Session.set('doctorNew', false);
	//	Session.set('doctorList', true);

        var skip = Session.get('skipDoctor');
        if (skip > -1) {
            return Meteor.subscribe('doctorsAll', CONSTANTS.SKIP_USERS, skip);
        }
    }
});

Router.route('pageDoctor', {
    path: '/doctors/:_id',
    waitOn: function() {
		Session.setDefault('skipDoctor', 0);
        Session.setDefault('less', true);
        Session.setDefault('more', false);
        Session.setDefault('searchName', false);

        Session.set('notFound', false);
        Session.set('resultOfSearch', false);
		
		var skip = Session.get('skipDoctor');
        if (skip > -1) {
			return [
				Meteor.subscribe('doctorsAll', CONSTANTS.SKIP_USERS, skip),
				Meteor.subscribe('doctorOne', this.params._id)
			]
		}
    },
    data: function() {
        return {
            doctorOne: Meteor.users.find({_id: this.params._id}).fetch()[0]
        };
    }
});

Router.route('newDoctor', {
    path: '/doctors-new',
	waitOn: function() {
		Session.setDefault('skipDoctor', 0);
        Session.setDefault('less', true);
        Session.setDefault('more', false);
        Session.setDefault('searchName', false);

        Session.set('notFound', false);
        Session.set('resultOfSearch', false);
		
		var skip = Session.get('skipDoctor');
        if (skip > -1) {
			return [
				Meteor.subscribe('doctorsAll', CONSTANTS.SKIP_USERS, skip)
			]
		}
    }
});

Router.route('intervalMatrix', {
	path: '/settings/interval_matrix',
	waitOn: function() {
		return [
			Meteor.subscribe('matrix')
		]
	},
	data: function() {
		Session.set('addMatrix', false);
		
		return {
			matrix: Interval.find().fetch()
		};
	}
});

Router.route('factorMatrix', {
	path: '/settings/factor_matrix',
	waitOn: function() {
		Session.set('durationBreaks', true);
		Session.set('addMatrix', false);
		Session.set('editMatrix', false);

		return [
			Meteor.subscribe('matrixByCode')
		]
	}
});

Router.onBeforeAction('loading');
