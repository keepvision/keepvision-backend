Meteor.publish('usersAll', function(limit, skip) {
	return Meteor.users.find({roles: {$in: ['user']}}, {limit: limit, skip: skip, sort: {"profile.last_name": 1}});
});

Meteor.publish('userOne', function(userId) {
	return Meteor.users.find({_id: userId});
});

Meteor.publish('children', function(userId) {
	return Children.find({user: {id: userId}});
});

Meteor.publish('childrenAll', function() {
	return Children.find();
});

Meteor.publish('child', function(childId) {
	return Children.find({_id: childId});
});

Meteor.publish('diagnosis', function(childId, limit, skip) {
	return Diagnosis.find({child: {id: childId}}, {limit: limit, skip: skip, sort: {date_begin: -1}});
});

Meteor.publish('session', function(childId, limit, skip) {
	return Ses.find({child: {id: childId}}, {limit: limit, skip: skip, sort: {date_begin: -1}});
});

Meteor.publish('matrix', function() {
	return Interval.find();
});

Meteor.publish('matrixByCode', function() {
	return Factor.find();
});

Meteor.publish('doctorsAll', function(limit, skip) {
    return Meteor.users.find({roles: {$in: ['doctor']}}, {limit: limit, skip: skip, sort: {"profile.last_name": 1}});
});

Meteor.publish('doctorOne', function(userId) {
    return Meteor.users.find({_id: userId});
});

Meteor.publish('adminsAll', function() {
    return Meteor.users.find({roles: {$in: ['admin']}});
});

Meteor.publish('adminOne', function(adminId) {
    return Meteor.users.find({_id: adminId});
});






















