Meteor.methods({
	saveAdmin: function (email, first_name, last_name) {
		if (!Meteor.userId()) {
		  throw new Meteor.Error('non-authorized');
		}
		var newUserId = Accounts.createUser({email: email, password : ''});
		
		Meteor.users.update(newUserId, {$set: {profile: {first_name: first_name, last_name: last_name}, date_update: new Date(), status: CONSTANTS.USER_STATUS_ACTIVE, ui_language: 'ru'}});
		
		var role = ['admin'];		
		return Roles.addUsersToRoles(newUserId, role);
	},
	
	/*editAdmin: function (adminId, email, status) {
        if (!Meteor.userId()) {
            throw new Meteor.Error('non-authorized');
        }
		
		Meteor.users.update(adminId, {
			$set: {
			   "status": status,
			   "date_update": new Date()
            }
        });
		return adminId;
    },*/
	
	deleteAdmin: function (id) {
        if (!Meteor.userId()) {
            throw new Meteor.Error('non-authorized');
        }
		if (id !== Meteor.userId()) {
			return Meteor.users.update(id, {
				$set: {
					status: CONSTANTS.DOCTOR_STATUS_REMOVE
				}
			});	
		}
		
		//return Meteor.users.remove(id);
    },

    saveDoctor: function (first_name, last_name, email, address, latitude, longitude, phone, website, type_doctor) {
        if (!Meteor.userId()) {
            throw new Meteor.Error('non-authorized');
        }
        var newUserId = Accounts.createUser({email: email, password : ''});
        Meteor.users.update(newUserId, {
            $set: {
                profile: {
                    first_name: first_name,
                    last_name: last_name,
					address: address,
					latitude: latitude,
					longitude: longitude,
					phone: phone,
					website: website,
					type_doctor: type_doctor
                },
                date_update: new Date(),
                status: CONSTANTS.DOCTOR_STATUS_ACTIVE,
                ui_language: 'ru'
            }
        });
        var role = ['doctor'];
        Roles.addUsersToRoles(newUserId, role);
        return newUserId;
    },
	
	editDoctor: function (doctorId, /*first_name, last_name, */email, address, latitude, longitude, phone, website, type_doctor) {
        if (!Meteor.userId()) {
            throw new Meteor.Error('non-authorized');
        }
		
		Meteor.users.update(doctorId, {
			$set: {
               "profile.address": address,
			   "profile.phone": phone,
			   "profile.website": website,
			   "profile.latitude": latitude,
			   "profile.longitude": longitude,
			   "profile.type_doctor": type_doctor,
			   "date_update": new Date()
            }
        });
		return doctorId;
    },
	
    deleteDoctor: function (id) {
        if (!Meteor.userId()) {
            throw new Meteor.Error('non-authorized');
        }
		
		return Meteor.users.update(id, {
			$set: {
				status: CONSTANTS.DOCTOR_STATUS_REMOVE
			}
		});	
		
		//return Meteor.users.remove(id);
    },
	
	saveMatrix: function (from, to, yellow, red) {
		if (!Meteor.userId()) {
		  throw new Meteor.Error('non-authorized');
		}	
		
		return Interval.insert({
			age_from: from,
			age_to: to,
			interval_yellow: yellow,
			interval_red: red
		});
		
	},
	
	editMatrix: function (id, from, to, yellow, red) {
		if (!Meteor.userId()) {
		  throw new Meteor.Error('non-authorized');
		}	
		
		return Interval.update(id, {
			$set: {
				age_from: from,
				age_to: to,
				interval_yellow: yellow,
				interval_red: red
			}
		});	
	},
	
	deleteMatrix: function (id) {
		if (!Meteor.userId()) {
		  throw new Meteor.Error('non-authorized');
		}
		
		return Interval.remove(id);
	},
	
	editMatrixFactor: function (id, age_from, age_to, value_from, value_to, factor) {
		if (!Meteor.userId()) {
		  throw new Meteor.Error('non-authorized');
		}	
		
		return Factor.update(id, {
			$set: {
				age_from: age_from,
				age_to: age_to,
				value_from: value_from,
				value_to: value_to,
				factor: factor
			}
		});	
	},
	
	saveMatrixFactor: function (age_from, age_to, value_from, value_to, factor, code, name) {
		if (!Meteor.userId()) {
		  throw new Meteor.Error('non-authorized');
		}	
		
		return Factor.insert({
			name: name,
			code: code,
			age_from: age_from,
			age_to: age_to,
			value_from: value_from,
			value_to: value_to,
			factor: factor
		});
		
	},
	
	deleteMatrixFactor: function (id) {
		if (!Meteor.userId()) {
		  throw new Meteor.Error('non-authorized');
		}
		
		return Factor.remove(id);
	},

	searchUser: function (name) {
		if (!Meteor.userId()) {
		  throw new Meteor.Error('non-authorized');
		}
		
		return Meteor.users.find({
			roles: {$in: ['user']}, 
			$or: [
				{"emails.address": {$regex: name + ".*", $options: 'i'}}, 
				{"profile.first_name": {$regex: name + ".*", $options: 'i'}}, 
				{"profile.last_name": {$regex: name + ".*", $options: 'i'}}
			]
		}).fetch();			
	}
});

























