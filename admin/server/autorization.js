Accounts.config({
	forbidClientAccountCreation:true
});

Accounts.validateLoginAttempt(function(par) {
	if (par.error) {
		throw new Meteor.Error(403, par.error.message);
	}
	if (!(Roles.userIsInRole(par.user, 'admin'))) {
		throw new Meteor.Error(403, "Нет прав");
	}
	return true;
});