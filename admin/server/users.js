/*Accounts.onCreateUser(function(options, user){
  user.roles = ['admin'];
  user.date_update = new Date();
  user.status = CONSTANTS.USER_STATUS_ACTIVE;
  user.ui_language = "ru";
  
  //var role = ['admin'];
  //Roles.addUsersToRoles(user, role);
  
  return user;
});*/

Meteor.publish("userData", function () {
  if (this.userId) {
    return Meteor.users.find({_id: this.userId});
  } else {
    this.ready();
  }
});