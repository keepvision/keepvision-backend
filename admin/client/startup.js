Meteor.startup(function () {
	var locale = window.navigator.userLanguage || window.navigator.language;
	moment.locale(locale);
	
	GoogleMaps.load();
  });