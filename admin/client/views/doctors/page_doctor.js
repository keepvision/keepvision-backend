Template.pageDoctor.helpers ({
	page: function(e) {
        return Session.get('skipDoctor')/CONSTANTS.SKIP_USERS + 1;
    },
    less: function() {
        var skip = Session.get('skipDoctor');

        if (skip > 0) {
            Session.set('less', false);
        } else {
            Session.set('less', true);
        }

        return Session.get('less');
    },
    more: function() {
        var t = Meteor.users.find().fetch();
        var arr = [];

        _.each(t, function (doctor) {
            if (Roles.userIsInRole(doctor._id, 'doctor')) {
                arr.push(doctor);
            }
        });

        if (arr.length < CONSTANTS.SKIP_USERS) {
            Session.set('more', true);
        } else {
            Session.set('more', false);
        }

        return Session.get('more');
    },
    doctor: function() {
        return Meteor.users.find({roles: {$in: ['doctor']}}).fetch();
    },
	type: function () {
		if (this.doctorOne.profile.type_doctor == 0) {
			return 'private';
		} 
		if (this.doctorOne.profile.type_doctor == 1) {
			return 'clinic';
		} 
    },
	othertype: function () {
		if (this.doctorOne.profile.type_doctor == 0) {
			return 'clinic';
		} 
		if (this.doctorOne.profile.type_doctor == 1) {
			return 'private';
		} 
    },
	date_update: function () {
		return moment(this.doctorOne.date_update).format("L LT");
    },
    createdAt: function () {
		return moment(this.doctorOne.createdAt).format("L LT");
    },
	
});

Template.registerHelper("initValue", function() {
	Session.set('doctor', this.doctorOne._id);
    return {
		first: this.doctorOne.profile.first_name,
		last: this.doctorOne.profile.last_name,
		address: this.doctorOne.profile.address,
		phone: this.doctorOne.profile.phone,
		email: this.doctorOne.emails[0].address,
		website: this.doctorOne.profile.website
	}
});

Template.pageDoctor.events({
	'click #page-next':function(e) {
        Session.set('skipDoctor', Session.get('skipDoctor') + CONSTANTS.SKIP_USERS);
    },
    'click #page-previous':function(e) {
        Session.set('skipDoctor', Session.get('skipDoctor') - CONSTANTS.SKIP_USERS);
    },
	
	'submit form':function(e) {
        //e.preventDefault();
		
        var /*first_name = $('[name = first_name]').val(),
            last_name = $('[name = last_name]').val(),*/
            email = $('[name = email]').val(),
			address = $('[name = address]').val(),
			latitude = $('[name = lat]').val(),
			longitude = $('[name = lng]').val(),
			phone = $('[name = phone]').val();
			website = $('[name = website]').val();
		
		if ($('#select').val() === 'clinic') {
            var type_doctor = 1;
		}
        if ($('#select').val() === 'private') {
            var type_doctor = 0;
		}  
		
        Meteor.call("editDoctor", Session.get('doctor'), /*first_name, last_name, */email, address, latitude, longitude, phone, website, type_doctor);
    }
});

















