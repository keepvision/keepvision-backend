Template.itemDoctor.helpers ({
    userId: function () {
        return this._id.valueOf();
    },
	status: function () {
		if (this.status == 0) {
			return 'активен';
		}
		if (this.status == 1) {
			return 'удален';
		}
    }
});

Template.itemDoctor.events({
    'click #delete': function(e) {
        Meteor.call("deleteDoctor", this._id);
    },
	'click #doctor-detail': function(e) {
		//Session.set('doctorMap', this._id);
		Router.go('pageDoctor',{ _id: this._id.valueOf()});
		//Session.set('doctorNew', false);
        //Session.set('doctor', this._id);
		//Session.set('doctorList', false);
    },
	
	'mouseenter .list-group-item': function (e) { 
		$('#' + this._id + ' .icon-preview').css('visibility', 'visible');
	},
	'mouseleave .list-group-item': function (e) { 
		$('#' + this._id + ' .icon-preview').css('visibility', 'hidden');
	}
});