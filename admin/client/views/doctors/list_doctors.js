Template.listDoctors.helpers({
    page: function(e) {
        return Session.get('skipDoctor')/CONSTANTS.SKIP_USERS + 1;
    },
    less: function() {
        var skip = Session.get('skipDoctor');

        if (skip > 0) {
            Session.set('less', false);
        } else {
            Session.set('less', true);
        }

        return Session.get('less');
    },
    more: function() {
        var t = Meteor.users.find().fetch();
        var arr = [];

        _.each(t, function (doctor) {
            if (Roles.userIsInRole(doctor._id, 'doctor')) {
                arr.push(doctor);
            }
        });

        if (arr.length < CONSTANTS.SKIP_USERS) {
            Session.set('more', true);
        } else {
            Session.set('more', false);
        }

        return Session.get('more');
    },
    doctor: function() {
        return Meteor.users.find({roles: {$in: ['doctor']}}).fetch();
    }//,
	/*choose: function() {
		var doctorId = Session.get('doctor');
        return Meteor.users.find({_id: doctorId}).fetch()[0];
    },
	email: function() {
		var doctorId = Session.get('doctor');
        var doc = Meteor.users.find({_id: doctorId}).fetch()[0];
		return doc.emails[0].address;
    },
	date_update: function () {
		var doctorId = Session.get('doctor');
        var doc = Meteor.users.find({_id: doctorId}).fetch()[0];
        return moment(doc.date_update).format("L LT");
    },
    createdAt: function () {
		var doctorId = Session.get('doctor');
        var doc = Meteor.users.find({_id: doctorId}).fetch()[0];
        return moment(doc.createdAt).format("L LT");
    },
	doctorNew: function () {
        return Session.get('doctorNew');
    },
	doctorList: function () {
        return Session.get('doctorList');
    }*/
});


Template.listDoctors.events({
    'click #page-next':function(e) {
        Session.set('skipDoctor', Session.get('skipDoctor') + CONSTANTS.SKIP_USERS);
    },
    'click #page-previous':function(e) {
        Session.set('skipDoctor', Session.get('skipDoctor') - CONSTANTS.SKIP_USERS);
    },
	'click .doctor-btn-new':function(e) {
        Router.go('newDoctor');
    }
});