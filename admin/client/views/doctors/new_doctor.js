Template.newDoctor.helpers ({
	page: function(e) {
        return Session.get('skipDoctor')/CONSTANTS.SKIP_USERS + 1;
    },
    less: function() {
        var skip = Session.get('skipDoctor');

        if (skip > 0) {
            Session.set('less', false);
        } else {
            Session.set('less', true);
        }

        return Session.get('less');
    },
    more: function() {
        var t = Meteor.users.find().fetch();
        var arr = [];

        _.each(t, function (doctor) {
            if (Roles.userIsInRole(doctor._id, 'doctor')) {
                arr.push(doctor);
            }
        });

        if (arr.length < CONSTANTS.SKIP_USERS) {
            Session.set('more', true);
        } else {
            Session.set('more', false);
        }

        return Session.get('more');
    },
    doctor: function() {
        return Meteor.users.find({roles: {$in: ['doctor']}}).fetch();
    },
	type: function () {
		if (this.doctorOne.profile.type_doctor == 0) {
			return 'private';
		} 
		if (this.doctorOne.profile.type_doctor == 1) {
			return 'clinic';
		} 
    },
	othertype: function () {
		if (this.doctorOne.profile.type_doctor == 0) {
			return 'clinic';
		} 
		if (this.doctorOne.profile.type_doctor == 1) {
			return 'private';
		} 
    },
	date_update: function () {
		return moment(this.doctorOne.date_update).format("L LT");
    },
    createdAt: function () {
		return moment(this.doctorOne.createdAt).format("L LT");
    }
});


/*Template.newDoctor.rendered = function() {
    $('.dropdown-menu').find('input').click(function (event) {
        event.stopPropagation();
    });
};*/
Template.newDoctor.events({
	/*'click .doctor-btn-save': function(e) {
        $('#authorizationForm').submit();
    },*/

	'submit form':function(e) {
        e.preventDefault();
		
        var first_name = $('[name = first_name]').val(),
            last_name = $('[name = last_name]').val(),
            email = $('[name = email]').val(),
			address = $('[name = address]').val(),
			latitude = $('[name = latitude]').val(),
			longitude = $('[name = longitude]').val(),
			phone = $('[name = phone]').val();
			website = $('[name = website]').val();
			
		if ($('#select').val() === 'clinic') {
            var type_doctor = CONSTANTS.DOCTOR_CLINIC;
		}
        if ($('#select').val() === 'private') {
            var type_doctor = CONSTANTS.DOCTOR_PRIVATE;
		}    
		
        Meteor.call("saveDoctor", first_name, last_name, email, address, latitude, longitude, phone, website, type_doctor, function (err, result) {
			if (result) {
				Router.go('pageDoctor',{ _id: result});
			}
		});
    }
});




