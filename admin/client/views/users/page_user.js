Template.pageUser.helpers ({
	page: function(e) {
		return Session.get('skipUser')/CONSTANTS.SKIP_USERS + 1;
	},
	less: function() {		
		var skip = Session.get('skipUser');
		
		if (skip > 0) {
			Session.set('less', false);
		} else {
			Session.set('less', true);
		}
		
		return Session.get('less');
	},
	more: function() {
		var t = Meteor.users.find().fetch();
		var arr = [];
		
		_.each(t, function (user) {
			if (Roles.userIsInRole(user._id, 'user')) {
				arr.push(user);
			}
		});
		
		if (arr.length < CONSTANTS.SKIP_USERS) {
			Session.set('more', true);
		} else {
			Session.set('more', false);
		}
		
		return Session.get('more');
	},
	notFound: function() {
		return Session.get('notFound');
	},
	resultOfSearch: function() {
		return Session.get('resultOfSearch');
	},	
	query: function() {
		return $('#name-search').val();
	},
	user: function() {
		var name = Session.get('searchName');
		
		if (!name) {
			var users = Meteor.users.find({roles: {$in: ['user']}}).fetch();
			Session.set('notFound', false);
			Session.set('resultOfSearch', false);
			return users;
		} else {
			Meteor.call("searchUser", name, function(err, result) {
				if (result.length === 0) {
					Session.set('notFound', true);
					Session.set('resultOfSearch', false);
				} else {
					Session.set('notFound', false);
					Session.set('resultOfSearch', result);
				}
			});
		}
	},
	search: function() {
		return Session.get('search');
	},
	email: function () {
		return this.userOne.emails[0].address;
	},
	date_update: function () {		
        return moment(this.userOne.date_update).format("L LT");
    },
    createdAt: function () {
		return moment(this.userOne.createdAt).format("L LT");
    }



	
/*	child: function () {
		var userId = Session.get('user');
		var t = Children.find({user: {id: userId}}).fetch()[0];
		return Children.find({user: {id: userId}});
	},*/

  
/*  userStatusRemove: function () {
    return Session.get('userStatusRemove');
  },
  userStatusActive: function () {
    return Session.get('userStatusActive');
  },*/
	
});

Template.pageUser.events({
	'click #page-next':function(e) {
		Session.set('skipUser', Session.get('skipUser') + CONSTANTS.SKIP_USERS);
    },
	'click #page-previous':function(e) {
		Session.set('skipUser', Session.get('skipUser') - CONSTANTS.SKIP_USERS);
    },
	'click #icon-search': function(e) {
		Session.set('search', !Session.get('search'));
	},
	'keyup #name-search': function(e) {
		Session.set('notFound', false);
		Session.set('resultOfSearch', false);
		
		var name = $('#name-search').val();
		if (name === '') {
			Session.set('searchName', false);
		} else {
			Session.set('searchName', name);
		}
	}
});
