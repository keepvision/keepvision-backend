Template.itemUser.helpers ({
  userId: function () {
    return this._id.valueOf();
  },
  email: function () {
    return this.emails[0].address;
  },
  first_name: function () {
    return this.profile.first_name;
  },
  last_name: function () {
    return this.profile.last_name;
  }
});

Template.itemUser.events({
/*  'click .list-group-item':function(e) {
    e.stopPropagation();
    Router.go('pageUser',{ _id: this._id.valueOf()});
  },*/
 /* 'click #user-detail': function(e) {
        Session.set('user', this._id);
		Session.set('userList', false);
    }*/
	'click #user-detail': function(e) {
		Router.go('pageUser',{ _id: this._id.valueOf()});
    }
});