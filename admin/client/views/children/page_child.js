Template.pageChild.helpers ({
	pageSession: function(e) {
		return Session.get('skipSession')/CONSTANTS.SKIP_SESSIONS + 1;
	},
	lessSession: function() {		
		var skip = Session.get('skipSession');
		
		if (skip > 0) {
			Session.set('lessSession', false);
		} else {
			Session.set('lessSession', true);
		}
		
		return Session.get('lessSession');
	},
	moreSession: function() {
		var t = Ses.find({child: {id: this.child._id}}).fetch();
		
		if (t.length < CONSTANTS.SKIP_SESSIONS) {
			Session.set('moreSession', true);
		} else {
			Session.set('moreSession', false);
		}
		
		return Session.get('moreSession');
	},
	pageDiagnosis: function(e) {
		return Session.get('skipDiagnosis')/CONSTANTS.SKIP_SESSIONS + 1;
	},
	lessDiagnosis: function() {		
		var skip = Session.get('skipDiagnosis');
		
		if (skip > 0) {
			Session.set('lessDiagnosis', false);
		} else {
			Session.set('lessDiagnosis', true);
		}
		
		return Session.get('lessDiagnosis');
	},
	moreDiagnosis: function() {
		var t = Diagnosis.find({child: {id: this.child._id}}).fetch();
		
		if (t.length < CONSTANTS.SKIP_SESSIONS) {
			Session.set('moreDiagnosis', true);
		} else {
			Session.set('moreDiagnosis', false);
		}
		
		return Session.get('moreDiagnosis');
	},
  birthday: function () {
    return moment(this.birthday).format("L")
  },
  genderFemale: function () {
    return Session.get('genderFemale');
  },
  genderMale: function () {
    return Session.get('genderMale');
  },
  goodParrent: function () {
    return Session.get('goodParrent');
  },
  badParrent: function () {
    return Session.get('badParrent');
  },
  date_update: function () {
    return moment(this.child.date_update).format("L LT")
  },
  sessionMore: function () {
	return Session.get('sessionMore');
  },
  diagnosisMore: function () {
	return Session.get('diagnosisMore');
  },
  email: function () {
	return this.parrent.emails[0].address;
  },
  createdAt: function () {
    return moment(this.parrent.createdAt).format("L LT")
  },
  date_update_par: function () {
    return moment(this.parrent.date_update).format("L LT")
  }
});

Template.pageChild.events({
	'click #page-next-s':function(e) {
		e.preventDefault();
		Session.set('skipSession', Session.get('skipSession') + CONSTANTS.SKIP_SESSIONS);
		Session.set('sessionMore', true);
		Session.set('diagnosisMore', false);
    },
	'click #page-previous-s':function(e) {
		e.preventDefault();
		Session.set('skipSession', Session.get('skipSession') - CONSTANTS.SKIP_SESSIONS);
		Session.set('sessionMore', true);
		Session.set('diagnosisMore', false);
    },
	'click #page-next-d':function(e) {
		e.preventDefault();
		Session.set('skipDiagnosis', Session.get('skipDiagnosis') + CONSTANTS.SKIP_SESSIONS);
		Session.set('diagnosisMore', true);
		Session.set('sessionMore', false);
    },
	'click #page-previous-d':function(e) {
		e.preventDefault();
		Session.set('skipDiagnosis', Session.get('skipDiagnosis') - CONSTANTS.SKIP_SESSIONS);
		Session.set('diagnosisMore', true);
		Session.set('sessionMore', false);
    },
	'click #diagnosis':function(e) {
		Session.set('diagnosis', true);
		Session.set('session', false);
		Session.set('sessionMore', false);
		Session.set('diagnosisMore', true);
    },
	'click #session':function(e) {
		Session.set('session', true);
		Session.set('diagnosis', false);
		Session.set('sessionMore', true);
		Session.set('diagnosisMore', false);
    },
	'click #child-btn-back':function(e) {
		//e.stopPropagation();
		Router.go('pageUser', {_id: this.parrent._id});
    }
});