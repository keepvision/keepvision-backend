Template.itemDiagnosis.helpers ({
  diagnosis_data: function () {
	return moment(this.date_update).format("L LT")
  },
  vision_safety: function () {
    return this.vision_safety.toFixed(2);
  }
});

Template.itemDiagnosis.events({
	'mouseenter tr': function (e) { 
		$('#' + this._id + ' .td-green').css('visibility', 'visible');
	},
	'mouseleave tr': function (e) { 
		$('#' + this._id + ' .td-green').css('visibility', 'hidden');
	},
	'click #more-info': function (e) { 
		//console.log(this.diagnosis_data.toString());
		//console.log(JSON.stringify(this.diagnosis_data));
		alert(JSON.stringify(this.diagnosis_data));
	}
});