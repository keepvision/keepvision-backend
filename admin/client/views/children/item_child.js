Template.itemChild.helpers ({
  name: function () {
    return this.name;
  }
});

Template.itemChild.events({
  'click .list-group-item':function(e) {
    e.stopPropagation();
    Router.go('pageChild',{ _id: this._id});
  }
});