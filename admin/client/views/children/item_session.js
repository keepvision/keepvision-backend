Template.itemSession.helpers ({
	begin: function () {
		return moment(this.date_begin).format("L LT")
	},
	green: function () {
		if (!this.date_yellow) {
			var res = this.date_end - this.date_begin;
		} else {
			var res = this.date_yellow - this.date_begin;
		}
		if (res !== 0) {
			var hour = parseInt(res / 3600000),
				min = parseInt((res - hour * 3600000) / 60000),
				sec = parseInt((res - hour * 3600000 - min*60000) / 1000);
			return hour + ':' + min + ':' + sec;
		}
	},
	yellow: function () {
		if (this.date_yellow) {
			if (!this.date_red) {
				var res = this.date_end - this.date_yellow;
			} else {
				var res = this.date_red - this.date_yellow;
			}
			if (res !== 0) {
				var hour = parseInt(res / 3600000),
					min = parseInt((res - hour * 3600000) / 60000),
					sec = parseInt((res - hour * 3600000 - min*60000) / 1000);
				return hour + ':' + min + ':' + sec;
			}
		}
	},
	red: function () {
		if (this.date_red) {
			var res = this.date_end - this.date_red;
		
			if (res !== 0) {
				var hour = parseInt(res / 3600000),
					min = parseInt((res - hour * 3600000) / 60000),
					sec = parseInt((res - hour * 3600000 - min*60000) / 1000);
				return hour + ':' + min + ':' + sec;
			}
		}
	},
	duration: function () {
		return this.duration;
	},
	interval: function () {
		return this.interval;
	}
});

Template.itemSession.events({
	'mouseenter tr': function (e) { 
		$('#' + this._id + ' .td-green').css('visibility', 'visible');
	},
	'mouseleave tr': function (e) { 
		$('#' + this._id + ' .td-green').css('visibility', 'hidden');
	}

});