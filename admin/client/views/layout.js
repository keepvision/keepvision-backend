Template.layout.rendered = function() {
	$.material.init();
	$('.dropdown-toggle').dropdown();
	$('.dropdown-menu').find('input').click(function (e) {
		e.stopPropagation();
	});
};

Template.layout.helpers({
	userName:function() {
		var user = Meteor.user();
		if (!user) return 'Войти/Зарегистрироваться';
		/*if (user.profile){
			return user.profile.name;
		}*/
		return user.emails[0].address;
	},
	typeMenuCreateUser:function() {
		return Session.get('typeMenuCreateUser');
	},
	typeMenuForgotPassword:function() {
		return Session.get('typeMenuForgotPassword');
	}
});