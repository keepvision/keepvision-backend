Template.createUser.rendered = function() {
	$('.dropdown-menu').find('input').click(function (event) {
		event.stopPropagation();
	});
};

Template.createUser.events({
	'click #create-account-buttons':function(e) {
		var email = $('#create-user-email').val(),
			password = $('#create-user-password').val();
		Accounts.createUser({email: email, password : password}, function(err){
			Session.set('typeMenuCreateUser', false);
			if (err) alert(err.reason);
		});
	},
	'click #cancel-buttons':function(event){
		event.stopPropagation();
		Session.set('typeMenuCreateUser', false);
	}
});