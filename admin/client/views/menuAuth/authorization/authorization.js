Template.authorization.rendered = function() {
  $('.dropdown-menu').find('input').click(function (event) {
    event.stopPropagation();
	});
};

Template.authorization.events({
  'click #forgot-password-link': function(event) {
    event.stopPropagation();
		Session.set('typeMenuForgotPassword', true);
	},
	'click #signup-link':function(event) {
    event.stopPropagation();
		Session.set('typeMenuCreateUser', true);
	},
/*	'click #login-buttons-password':function(e) {
		var email = $('#authorization-login-email').val(),
			password = $('#authorization-login-password').val();
		Meteor.loginWithPassword(email, password, function(err){
			if (err) alert(err.reason);
		});	
	}*/
	'submit form':function(e) {
		e.preventDefault();
		var email = $('[name = email]').val(), 
			password = $('[name = password]').val();
			
		Meteor.loginWithPassword(email, password, function(err){
			if (err) alert(err.reason);
		});	
	}
});