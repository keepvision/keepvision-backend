Template.passwordRecoveryDialog.events({
	'submit form':function(e) {
		e.preventDefault();
		var password = $(e.currentTarget).find('[name = password]').val(),
		token = Session.get('tokenReserPassword');
		Accounts.resetPassword(token, password);
		Modal.hide('passwordRecoveryDialog');
		Router.go('appList', {statusApp:-1});
	},
	'click .btn-close':function() {
		Modal.hide('passwordRecoveryDialog');
	}
});