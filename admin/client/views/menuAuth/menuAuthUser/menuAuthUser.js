Template.menuAuthUser.events({
	'click #login-buttons-logout':function(event) {
		Meteor.logout();
		Router.go('mainPage');
	},
	'click #change-password':function(event) {
		Modal.show('passwordChangeDialog');
	},
	'click #change-profile':function(event) {
		Router.go('pagePersonal');
	}
});