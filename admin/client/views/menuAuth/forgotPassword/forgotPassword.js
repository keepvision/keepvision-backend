Template.forgotPassword.rendered = function() {
	$('.dropdown-menu').find('input').click(function (event) {
		event.stopPropagation();
	});
}

Template.forgotPassword.events({
	'click #cancel-buttons':function(e) {
		e.stopPropagation();
		Session.set('typeMenuForgotPassword', false);
	},
	'click #login-buttons-forgot-password':function(e) {
		var email = $('#forgot-password-email').val();
		Accounts.forgotPassword({email: email}, function(err) {
		Session.set('typeMenuForgotPassword', false);
		if (err) alert(err.reason);
		else alert('Please check your email');
    });
	}
});