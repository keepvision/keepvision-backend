Template.intervalMatrix.helpers({
	addMatrix: function () {
		return Session.get('addMatrix');
	}
});
Template.intervalMatrix.events({
	'click #add-matrix': function(e) {
		Session.set('addMatrix', true);
		Session.set('editMatrix', false);
	},
	
	'submit form#newMatrix':function(e) {
		e.preventDefault();
		
		var from = $('#newMatrix [name = age_from]').val(), 
			to = $('#newMatrix [name = age_to]').val(), 
			yellow = $('#newMatrix [name = yellow]').val(),
			red = $('#newMatrix [name = red]').val();
			
		Meteor.call("saveMatrix", from, to, yellow, red);
		Session.set('addMatrix', false);	
	},
	
	'click #cancel-matrix': function(e) {
		Session.set('addMatrix', false);		
	}
});
