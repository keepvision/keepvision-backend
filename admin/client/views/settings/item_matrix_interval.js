Template.itemMatrix.helpers({
	editMatrix: function () {
		return Session.get('editMatrix') === this._id;
	}
});

Template.itemMatrix.events({
	'click #delete': function(e) {
		Meteor.call("deleteMatrix", this._id);	
	},
	'click #cancel-matrix': function(e) {
		Session.set('editMatrix', false);		
	},
	
	'click #matrix-input': function(e) {	
		Session.set('editMatrix', this._id);
	},
	
	'submit form':function(e) {
        e.preventDefault();
		
		var from = $('#' + this.id + ' [name = age_from]').val(),
			to = $('#' + this.id + ' [name = age_to]').val(),
			yellow = $('#' + this.id + ' [name = yellow]').val(),
			red = $('#' + this.id + ' [name = red]').val();
		
		Meteor.call("editMatrix", this.id, from, to, yellow, red);
		Session.set('editMatrix', false);	
    },
	
	'mouseenter tr': function (e) { 
		$('#' + this._id + ' .icon-preview').css('visibility', 'visible');
	},
	'mouseleave tr': function (e) { 
		$('#' + this._id + ' .icon-preview').css('visibility', 'hidden');
	}
});

Template.registerHelper("initMatrix", function() {
	return {
		age_from: this.age_from,
		age_to: this.age_to,
		interval_yellow: this.interval_yellow,
		interval_red: this.interval_red
	}
});
