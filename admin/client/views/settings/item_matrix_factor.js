Template.itemMatrixFactor.helpers({
	editMatrix: function () {
		return Session.get('editMatrix') === this._id;
	}
});
Template.itemMatrixFactor.events({
	'click #delete': function(e) {
		Meteor.call("deleteMatrixFactor", this._id);	
	},
	'click #cancel-matrix': function(e) {
		Session.set('editMatrix', false);		
	},
	
	'click #matrix-input': function(e) {	
		Session.set('editMatrix', this._id);
	},
	
	'submit form':function(e) {
        e.preventDefault();
		
		var age_from = $('#' + this.id + ' [name = age_from]').val(),
			age_to = $('#' + this.id + ' [name = age_to]').val(),
			value_from = $('#' + this.id + ' [name = value_from]').val(),
			value_to = $('#' + this.id + ' [name = value_to]').val(),
			factor = $('#' + this.id + ' [name = factor]').val();
		
		Meteor.call("editMatrixFactor", this.id, age_from, age_to, value_from, value_to, factor);
		Session.set('editMatrix', false);	
    },
	
	'mouseenter tr': function (e) { 
		$('#' + this._id + ' .icon-preview').css('visibility', 'visible');
	},
	'mouseleave tr': function (e) { 
		$('#' + this._id + ' .icon-preview').css('visibility', 'hidden');
	}

});

Template.registerHelper("initMatrixFactor", function() {
	return {
		age_from: this.age_from,
		age_to: this.age_to,
		value_from: this.value_from,
		value_to: this.value_to,
		factor: this.factor
	}
});










