Template.factorMatrix.helpers({
	addMatrix: function () {
		return Session.get('addMatrix');
	},
	durationBreaks: function () {
		return Session.get('durationBreaks');
	},
	dailyUse: function () {
		return Session.get('dailyUse');
	},
	visualAcuity: function () {
		return Session.get('visualAcuity');
	},
	gender: function () {
		return Session.get('gender');
	},
	heredity: function () {
		return Session.get('heredity');
	},
	lighting: function () {
		return Session.get('lighting');
	},
	
	matrix: function() {
		var code;
		if (Session.get('durationBreaks')) code = 0;
		if (Session.get('dailyUse')) code = 1;
		if (Session.get('visualAcuity')) code = 2;
		if (Session.get('gender')) code = 3;
		if (Session.get('heredity')) code = 4;
		if (Session.get('lighting')) code = 5;
		
		return Factor.find({code: code}).fetch();
	}
});

Template.factorMatrix.events({
	'click #add-matrix': function(e) {
		Session.set('addMatrix', true);
		Session.set('editMatrix', false);
	},
	
	'submit form#newMatrixFactor':function(e) {
		e.preventDefault();
		
		var age_from = $('#newMatrixFactor [name = age_from]').val(), 
			age_to = $('#newMatrixFactor [name = age_to]').val(), 
			value_from = $('#newMatrixFactor [name = value_from]').val(),
			value_to = $('#newMatrixFactor [name = value_to]').val(),
			factor = $('#newMatrixFactor [name = factor]').val();
		
		if (Session.get('durationBreaks')) {
			var code = CONSTANTS.FACTOR_MATRIX_DURATION_BREAKS_CODE,
				name = 'Продолжительность перерывов';
			}
		if (Session.get('dailyUse')) {
			var code = CONSTANTS.FACTOR_MATRIX_DURATION_USE_CODE,
				name = 'Ежедневное пользование';
		}
		if (Session.get('visualAcuity')) {
			var code = CONSTANTS.FACTOR_MATRIX_SHARPNESS_VISION_CODE,
				name = 'Острота зрения';
		}
		if (Session.get('gender')) {
			var code = CONSTANTS.FACTOR_MATRIX_GENDER_CODE,
				name = 'Пол';
		}
		if (Session.get('heredity')) {
			var code = CONSTANTS.FACTOR_MATRIX_HEREDITY_CODE,
				name = 'Наследственность';
		}
		if (Session.get('lighting')) {
			var code = CONSTANTS.FACTOR_MATRIX_LIGHTING_CODE,
				name = 'Освещение';
		}
			
		Meteor.call("saveMatrixFactor", age_from, age_to, value_from, value_to, factor, code, name);
		Session.set('addMatrix', false);	
	},
	
	'click #cancel-matrix': function(e) {
		Session.set('addMatrix', false);		
	},
	
	'click #durationBreaks': function(e) {
		Session.set('durationBreaks', true);
		Session.set('dailyUse', false);
		Session.set('visualAcuity', false);
		Session.set('gender', false);
		Session.set('heredity', false);
		Session.set('lighting', false);	
	},
	'click #dailyUse': function(e) {
		Session.set('dailyUse', true);
		Session.set('durationBreaks', false);
		Session.set('visualAcuity', false);
		Session.set('gender', false);
		Session.set('heredity', false);
		Session.set('lighting', false);
	},
	'click #visualAcuity': function(e) {
		Session.set('visualAcuity', true);
		Session.set('durationBreaks', false);
		Session.set('dailyUse', false);
		Session.set('gender', false);
		Session.set('heredity', false);
		Session.set('lighting', false);
	},	
	'click #gender': function(e) {
		Session.set('gender', true);
		Session.set('durationBreaks', false);
		Session.set('dailyUse', false);
		Session.set('visualAcuity', false);
		Session.set('heredity', false);
		Session.set('lighting', false);
	},
	'click #heredity': function(e) {
		Session.set('heredity', true);
		Session.set('durationBreaks', false);
		Session.set('dailyUse', false);
		Session.set('visualAcuity', false);
		Session.set('gender', false);
		Session.set('lighting', false);
	},	
	'click #lighting': function(e) {
		Session.set('lighting', true);
		Session.set('durationBreaks', false);
		Session.set('dailyUse', false);
		Session.set('visualAcuity', false);
		Session.set('gender', false);
		Session.set('heredity', false);
	}
});