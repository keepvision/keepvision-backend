Template.mapNew.helpers({
	mapOptions: function() {
		if (GoogleMaps.loaded()) {
			return {
				center: new google.maps.LatLng(55.7535, 37.6092),
				zoom: 8
			};
		}
	}
});

Template.mapNew.onCreated(function() {
	GoogleMaps.ready('mapNew', function(map) {		
		var myLatlng = new google.maps.LatLng(55.7535, 37.6092);
		var markers = [];
		var marker = new google.maps.Marker({
			position: myLatlng,
		});
		markers.push(marker);
		marker.setMap(map.instance);
		
		google.maps.event.addListener(map.instance, 'click', function(event) {
			markers[0].setMap(null);
			
			var marker = new google.maps.Marker({
				position: new google.maps.LatLng(event.latLng.lat(),event.latLng.lng()),
			});
			markers[0] = marker;
			marker.setMap(map.instance);
			
			$('[name = latitude]').val(event.latLng.lat());
			$('[name = longitude]').val(event.latLng.lng());
		});
    });
});
