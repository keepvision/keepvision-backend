Template.map.helpers({
	mapOptions: function() {
		Session.set('doctorMap', this.doctorOne);
		
		if (GoogleMaps.loaded()) {
			return {
				center: new google.maps.LatLng(this.doctorOne.profile.latitude, this.doctorOne.profile.longitude),
				zoom: 16
			};
		}
	}
});

Template.map.onCreated(function() {
	GoogleMaps.ready('map', function(map) {
		var doctor = Session.get('doctorMap');
		
		var myLatlng = new google.maps.LatLng(doctor.profile.latitude,doctor.profile.longitude);
		
		var markers = [];
		var marker = new google.maps.Marker({
			position: myLatlng,
		});
		markers.push(marker);
		marker.setMap(map.instance);
		
		
		google.maps.event.addListener(map.instance, 'click', function(event) {
			markers[0].setMap(null);
			var marker = new google.maps.Marker({
				position: new google.maps.LatLng(event.latLng.lat(),event.latLng.lng()),
			});
			markers[0] = marker;
			marker.setMap(map.instance);
			
			$('[name = lat]').val(event.latLng.lat());
			$('[name = lng]').val(event.latLng.lng());
			
		});
	});
});
