Template.itemAdmin.helpers ({
	status: function () {
		if (this.status == 0) {
			return 'активен';
		}
		if (this.status == 1) {
			return 'удален';
		}
    }
});

Template.itemAdmin.events({
    'click #delete': function(e) {
        Meteor.call("deleteAdmin", this._id);
    },
	
	'click #admin-detail': function(e) {
		Router.go('pageAdmin',{ _id: this._id.valueOf()});
    },
	
	'mouseenter .list-group-item': function (e) { 
		$('#' + this._id + ' .icon-preview').css('visibility', 'visible');
	},
	'mouseleave .list-group-item': function (e) { 
		$('#' + this._id + ' .icon-preview').css('visibility', 'hidden');
	}
});