Template.newAdmin.helpers ({
	admin: function() {
        return Meteor.users.find({roles: {$in: ['admin']}}).fetch();
    },
	letterSend: function() {
		Session.get('letterSend');
	}
	/*,
	date_update: function () {
		return moment(this.doctorOne.date_update).format("L LT");
    },
    createdAt: function () {
		return moment(this.doctorOne.createdAt).format("L LT");
    }*/
});

Template.newAdmin.events({
	/*'click .doctor-btn-save': function(e) {
        $('#authorizationForm').submit();
    },*/

	'submit form':function(e) {
        e.preventDefault();
		
        var first_name = $('[name = first_name]').val(),
            last_name = $('[name = last_name]').val(),
            email = $('[name = email]').val();
			
		Meteor.call("saveAdmin", email, first_name, last_name);
		Accounts.forgotPassword({email: email}, function(err, res) {
			if (err) {
				alert(err.reason);
				console.log(err);
			}
			else {
				//alert('Please check your email');
				Session.set('letterSend', true);
				console.log(res);
				console.log(Session.get('letterSend'));
			}
		});	
			
			
			
	/*	Meteor.call("saveDoctor", first_name, last_name, email, address, latitude, longitude, phone, website, type_doctor, function (err, result) {
			if (result) {
				Router.go('pageDoctor',{ _id: result});
			}
		});*/
    }
});




