App.setPreference("Orientation", "portrait");
App.accessRule("*"); 

App.info({
  // id: '',
  name: 'KeepVision',
   description: 'KeepVision for parent',
   version: '0.8.1'
  // author: '',
  // email: '',
  // website: ''
});


App.icons({
 
  // Android
  'android_ldpi': 'resources/icons/icon-36x36.png',
  'android_mdpi': 'resources/icons/icon-48x48.png',
  'android_hdpi': 'resources/icons/icon-72x72.png',
  'android_xhdpi': 'resources/icons/icon-96x96.png'
});

App.launchScreens({
  // Android
  'android_ldpi_portrait': 'resources/splash/splash-200x320.png',
  'android_mdpi_portrait': 'resources/splash/splash-320x480.png',
  'android_hdpi_portrait': 'resources/splash/splash-480x800.png',
  'android_xhdpi_portrait': 'resources/splash/splash-720x1280.png'
});