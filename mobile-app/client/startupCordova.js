if (Meteor.isCordova) {
	Meteor.startup( function() {
		document.addEventListener("backbutton", function() {
			if (document.location.pathname === "/"  || document.location.pathname === "/authorization")
				navigator.app.exitApp();
			else
				history.go(-1);
		});
	});
}

Meteor.startup(function(){
	moment.locale('en');
});

