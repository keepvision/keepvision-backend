Template.changePassword.events({
	'submit form':function(e) {
		e.preventDefault();
		if (!Meteor.status().connected) {
			Session.set('messageServer', 'Server is unavailable');
			return;
		}

		var oldPassword = $('input[name=passwordOld]').val(),
		newPassword = $('input[name=password]').val();

		Accounts.changePassword(oldPassword, newPassword, function(err) {
        if (!err) {
        	window.localStorage.setItem('password', newPassword);
        	Session.set('messageServer', 'Password successfully changed ');
        }
        else Session.set('messageServer', err.reason);
    });
	},
	'click .back':function(e) {
		e.preventDefault();
		history.back();
	}
});

Template.changePassword.helpers({
	messageServer:function() {
		return Session.get('messageServer');
	}
});