var emailMem = '';

Template.authorization.onRendered(function(){
	 var $tabs = $("#tabs"),
        $header = $("#header");

    $.material.init();

    $tabs.find(".tab").on("click", function(){
        var $tab = $(this),
            screen = $tab.data("target");

        if(!$tab.hasClass("active")){
            $tab.siblings(".active").removeClass("active");
            $tab.addClass("active");
        }

        if(screen == 'login'){
        	 	Session.set('messageServer', '');
            $(".screen.signup").fadeOut();
            $(".forgot-form").fadeOut();
            $(".screen.login").fadeIn().find(".login-form").fadeIn();
        } else{
        	 	Session.set('messageServer', '');
            $(".screen.login").fadeOut();
            $(".forgot-form").fadeOut();
            $(".screen.signup").fadeIn();
        }

        $(".tab_panel:visible").fadeOut();
        $(".tab_panel." + screen).fadeIn();
    });

    $(".forgot_password").on("click", function(){
    		Session.set('messageServer', '');
        $(".screen.login").find(".login-form").fadeOut();
        $(".forgot-form").fadeIn();
        $(".screen.signup").fadeOut();
    });
});

Template.authorization.events({
	'submit #authorizationForm':function(e) {
		e.preventDefault();
		if (!Meteor.status().connected) {
			Session.set('messageServer', 'Server is unavailable');
			return;
		}
		var email = $(e.target).find('[name=email]').val(),
		password = $(e.target).find('[name=password]').val();

		Meteor.loginWithPassword(email, password, function(err) {
        if (!err) {
        	window.localStorage.setItem('email', email);
        	window.localStorage.setItem('password', password);
        	Router.go('listChild');
        }
        else Session.set('messageServer', err.reason);
    });
	},
	'submit #registrationForm':function(e) {
		e.preventDefault();
		if (!Meteor.status().connected) {
			Session.set('messageServer', 'Server is unavailable');
			return;
		}
		var email = $(e.target).find('[name=email]').val(),
		password = $(e.target).find('[name=password]').val(),
		last_name =$(e.target).find('[name=last_name]').val(),
		first_name = $(e.target).find('[name=first_name]').val();

		Accounts.createUser({
			email: email, 
			password : password,
			profile:{
				first_name:first_name,
				last_name:last_name
			}
		}, function(err) {
			if (err) Session.set('messageServer', err.reason);
			else {
				window.localStorage.setItem('email', email);
				window.localStorage.setItem('password', password);
				Router.go('listChild');
			}
		});
	},
	'submit #forgotPasswordForm':function(e) {
		e.preventDefault();
		if (!Meteor.status().connected) {
			Session.set('messageServer', 'Server is unavailable');
			return;
		}
		var email = $(e.target).find('[name=email]').val();
		Meteor.call('resetPasswordUser', email, function(error) {
			if(error) Session.set('messageServer', error.reason);
			else Session.set('messageServer', 'New password send to e-mail' );
		});
	}
});

Template.authorization.helpers({
	messageServer:function() {
		return Session.get('messageServer');
	},
	emailValue:function() {
		return emailMem;
	}
});
