viewScreenUseDay = {};
viewScreenUseMonth = {};
scrollScreens = {};

scrollScreens.initialScreens = function(data, template, parent, viewScreenUse, typeReport) {
  if (!(data && data.childId && parent)) return;

 viewScreenUse.typeReport = typeReport;
 viewScreenUse.template = template;
 viewScreenUse.childId = data.childId;
 var child = Collection.child.findOne(data.childId);
 if (child) viewScreenUse.name = child.name;

 viewScreenUse.dateCenterScreen = new Date(Date.parse(data.date));

 if (parent)  viewScreenUse.leftScreen = Blaze.renderWithData(template, {
  name: viewScreenUse.name,
  date: getPrevDate(viewScreenUse)
 }, parent);
   viewScreenUse.leftScreen.firstNode().classList.add('left-screen');

   if (parent) viewScreenUse.centerScreen = Blaze.renderWithData(template, {
    name: viewScreenUse.name,
    date: viewScreenUse.dateCenterScreen
  }, parent);
    viewScreenUse.centerScreen.firstNode().classList.add('center-screen');

  if (parent) viewScreenUse.rightScreen =  Blaze.renderWithData(template, {
    name: viewScreenUse.name,
    date: getNextDate(viewScreenUse)
  }, parent);
  viewScreenUse.rightScreen.firstNode().classList.add('right-screen');
}


 scrollScreens.scrollLeft = function(viewScreenUse, parent, scrollChange) { 
    viewScreenUse.dateCenterScreen = getNextDate(viewScreenUse);
    //Удаляем левый экран
    var leftScreen = viewScreenUse.leftScreen;
    Meteor.setTimeout(function() {
      Blaze.remove(leftScreen);
    },1);
    //Центральный экран занимает позицию левого
    viewScreenUse.centerScreen.firstNode().classList.add('left-screen');
    viewScreenUse.centerScreen.firstNode().classList.remove('center-screen');
    viewScreenUse.leftScreen = viewScreenUse.centerScreen;
    //Правый экран занимает позицию центрального
    viewScreenUse.rightScreen.firstNode().classList.add('center-screen');
    viewScreenUse.rightScreen.firstNode().classList.remove('right-screen');
    viewScreenUse.centerScreen = viewScreenUse.rightScreen;
    //Создаем новый правый экран
    Meteor.setTimeout(function(){
      viewScreenUse.rightScreen = addScreenRight(viewScreenUse, parent);
      viewScreenUse.rightScreen.firstNode().classList.add('right-screen');
    },1);
}


scrollScreens.scrollRight = function(viewScreenUse, parent) {
   viewScreenUse.dateCenterScreen = getPrevDate(viewScreenUse);
   //Удаляем правый экран 
   var rightScreen = viewScreenUse.rightScreen;
   Meteor.setTimeout(function(){
    Blaze.remove(rightScreen);
   },1);
   //Центральный экран занимает позицию правого
   viewScreenUse.centerScreen.firstNode().classList.add('right-screen');
   viewScreenUse.centerScreen.firstNode().classList.remove('center-screen');
   viewScreenUse.rightScreen = viewScreenUse.centerScreen;
   //Левый  экран занимает позицию центрального
   viewScreenUse.leftScreen.firstNode().classList.add('center-screen');
   viewScreenUse.leftScreen.firstNode().classList.remove('left-screen');
   viewScreenUse.centerScreen = viewScreenUse.leftScreen;
   //Создаем новый левый экран
   Meteor.setTimeout(function(){
    viewScreenUse.leftScreen = addScreenLeft(viewScreenUse, parent);
    viewScreenUse.leftScreen.firstNode().classList.add('left-screen');
   },1);
}

function addScreenRight(viewScreenUse, parent) {
  if (parent) return  Blaze.renderWithData( viewScreenUse.template, {
    name: viewScreenUse.name,
    date: getNextDate(viewScreenUse)
  }, parent);
}

function addScreenLeft(viewScreenUse, parent) {
  if (parent) return  Blaze.renderWithData( viewScreenUse.template, {
    name: viewScreenUse.name,
    date: getPrevDate(viewScreenUse)
  }, parent, parent.children[0] );
}


function getNextDate(viewScreenUse) { 
	switch(viewScreenUse.typeReport) {
		case CONSTANTS.TYPE_REPORT_DAY: 
			return utilLib.nextDay(viewScreenUse.dateCenterScreen)
		break;
		case CONSTANTS.TYPE_REPORT_MONTH:
			return utilLib.nextMonth(viewScreenUse.dateCenterScreen);
		break;
	}
}

function getPrevDate(viewScreenUse) { 
	switch(viewScreenUse.typeReport) {
		case CONSTANTS.TYPE_REPORT_DAY: 
			return utilLib.prevDay(viewScreenUse.dateCenterScreen)
		break;
		case CONSTANTS.TYPE_REPORT_MONTH:
			return utilLib.prevMonth(viewScreenUse.dateCenterScreen);
		break;
	}
}
