symbolDiagnosis = {};
symbolDiagnosis.draw = function(instance, diagnosis, reportType) {
	widthBand = $('.actives').width();
	instance.$('.symbolDiagnosis').remove();
	drawWithTimeout(0, instance, diagnosis, reportType);
}

function drawWithTimeout(numbDiagnosis, instance, diagnosis, reportType) {
	var TIMEOUT = 2;
	if (diagnosis[numbDiagnosis]) {
	switch(reportType) {		
	 case 'day':
		drawDay(instance, diagnosis[numbDiagnosis]);
	break;
	case 'month':
		drawMonth(instance, diagnosis[numbDiagnosis]);
	break;
	}
	
	Meteor.setTimeout(function(){drawWithTimeout(numbDiagnosis+1, instance, diagnosis, reportType)}, TIMEOUT);
	}
}


function drawDay(instance, diagnos) {
	var MINUTE_IN_HOUR = 60,
	pixInMinute = widthBand / MINUTE_IN_HOUR,
	hour = diagnos.date_update.getHours(),
	minute = diagnos.date_update.getMinutes(),
  parent = instance.$('.' + hour),
  symbolDiv = document.createElement("div");
	symbolDiv.id = diagnos._id.valueOf();
	symbolDiv.classList.add("button");
  symbolDiv.classList.add("eye");
  symbolDiv.style.cssText = "left:"+ Math.floor(minute * pixInMinute) +"px";
	parent.append(symbolDiv);
}

function drawMonth(instance, diagnos) {
	var MINUTE_IN_HOUR = 60,
	MINUTE_IN_DAY = 1440,
	pixInMinute = widthBand / MINUTE_IN_DAY,
	day = diagnos.date_update.getDate(),
	hour = diagnos.date_update.getHours(),
	minute = diagnos.date_update.getMinutes(),
  parent = instance.$('.' + day),
  symbolDiv = document.createElement("div");
  symbolDiv.id = diagnos._id.valueOf();
  symbolDiv.classList.add("button");
  symbolDiv.classList.add("eye");
  symbolDiv.style.cssText = "left:"+ Math.floor((MINUTE_IN_HOUR * hour + minute) * pixInMinute) +"px";
	parent.append(symbolDiv);
}