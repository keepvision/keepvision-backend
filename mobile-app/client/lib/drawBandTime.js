	 
/**
 * findGreenZone() возвращает объект содержащий начало и конец интервала 
 * зеленой зоны сессии. Если определить не удалось, то возвращает false
 *
 * @param <Object> session
 * @return <Oject> greenZone
 */
function findGreenZone(session) {
	if (!(session && session.date_begin)) return false;
	return {
		beginInterval:session.date_begin,
		endInterval: session.date_yellow?session.date_yellow:session.date_end
	}
}

/**
 * findYellowZone() возвращает объект содержащий начало и конец интервала 
 * желтой зоны сессии. Если определить не удалось, то возвращает false
 *
 * @param <Object> session
 * @return <Oject> yellowZone
 */
function findYellowZone(session) {
	if (!(session && session.date_yellow)) return false;
	return {
		beginInterval: session.date_yellow,
		endInterval: session.date_red?session.date_red:session.date_end
	}
}

/**
 * findRedZone() возвращает объект содержащий начало и конец интервала 
 * красной зоны сессии. Если определить не удалось, то возвращает false
 *
 * @param <Object> session
 * @return <Oject> redZone
 */
function findRedZone(session) {
	if (!(session && session.date_red)) return false;
	return {
		beginInterval: session.date_red,
		endInterval: session.date_end
	}
}

function createBlock(parent, color, left , width) { 
	 if (!width) width = 1;
	 var block = document.createElement("div");
	 block.classList.add('day_active');
	 block.classList.add(color);
	 block.style.cssText = "left:"+ left +"px"+";width:"+ width +"px;";
	 parent.append(block);
}

/**
 * drawBlockDay() Рисует блок по интервалу из zone за сутки.
 *
 * @param <Object> instance
 * @param <Object> zone
 * @param <String> color
 */
function drawBlockDay(instance, zone, color) {
	 if (!instance) return; 
	 var MINUTE_IN_HOUR = 60,
	 pixInMinute = parseInt(widthBand / MINUTE_IN_HOUR, 10),
	 blockLeft,
	 blockWidth;
	 if (!(pixInMinute && zone)) return;
	 var hourBegin = zone.beginInterval.getHours(),
	 hourEnd = zone.endInterval.getHours();
	 if (hourBegin <= 7) hourBegin+= 24;
	 if (hourEnd <= 7) hourEnd+= 24;
	 var lengthZoneHour = hourEnd - hourBegin + 1;	
	 
	if (lengthZoneHour === 1) {// вся зона входит в одну полосу
		 blockLeft = Math.floor(zone.beginInterval.getMinutes() * pixInMinute);
		 blockWidth = Math.floor((zone.endInterval.getMinutes() - zone.beginInterval.getMinutes()) * pixInMinute);
		 var parent = instance.$('.'+hourBegin);
		 createBlock(parent, color, blockLeft, blockWidth);
	} 
	else {
		for (var i = 0; i < lengthZoneHour; i++) { // зона разделена на несколько полос
			if (i === 0) {
				 blockLeft = Math.floor(zone.beginInterval.getMinutes() * pixInMinute);
				 blockWidth =  widthBand - blockLeft;
				 parent = instance.$('.'+hourBegin);
			} else if (i === lengthZoneHour - 1) {
				 blockLeft = 1;
				 blockWidth = Math.floor(zone.endInterval.getMinutes() * pixInMinute);
				 parent = instance.$('.'+hourEnd);
			}
			else {
				 parent = instance.$('.'+(hourBegin+i));
				 blockLeft = 1;
				 blockWidth = widthBand;
			} 
			createBlock(parent, color, blockLeft, blockWidth);
		}
	}
}

/**
 * drawBlock() Рисует блок по интервалу из zone за месяц
 *
 * @param <Object> instance
 * @param <Object> zone
 * @param <String> color
 */
function drawBlockMonth(instance, zone, color) {
	 if (!instance) return; 
	 var MINUTE_IN_DAY = 1440,
	 MINUTE_IN_HOUR = 60,
	 MS_IN_MINUTE = 60000,
	 // widthBand = $('.actives').width(),
	 pixInMinute = widthBand / MINUTE_IN_DAY,
	 blockLeft,
	 blockWidth;
	 if (!(pixInMinute && zone)) return;
	 var dayBegin = zone.beginInterval.getDate(),
	 dayEnd = zone.endInterval.getDate();
	 var lengthZoneDay = dayEnd - dayBegin + 1;	
	
	if (lengthZoneDay === 1) {// вся зона входит в одну полосу

		 blockLeft = Math.floor((zone.beginInterval.getHours() * MINUTE_IN_HOUR + zone.beginInterval.getMinutes()) * pixInMinute);
		 blockWidth = Math.floor( (zone.endInterval - zone.beginInterval) /  MS_IN_MINUTE * pixInMinute);
		 parrent = instance.$('.'+dayBegin);
		 createBlock(parrent, color, blockLeft, blockWidth);
	} 
	else {
		for (var i = 0; i < lengthZoneDay; i++) { // зона разделена на несколько полос
			if (i === 0) {
				 blockLeft = Math.floor((zone.beginInterval.getHours() * MINUTE_IN_HOUR + zone.beginInterval.getMinutes()) * pixInMinute);
				 blockWidth =  widthBand - blockLeft;
				 parrent = instance.$('.'+dayBegin);
			} else if (i === lengthZoneDay - 1) {
				 blockLeft = 1;
				 blockWidth = Math.floor((zone.endInterval.getHours() * MINUTE_IN_HOUR + zone.endInterval.getMinutes()) * pixInMinute);
				 parrent = instance.$('.'+dayEnd);
			}
			else {
				 parrent = instance.$('.'+(dayBegin+i));
				 blockLeft = 1;
				 blockWidth = widthBand;
			} 
			createBlock(parrent, color, blockLeft, blockWidth);
		}
	}
}

bandTime = {};
bandTime.draw = function(instance, session, bandType) { 	
	 instance.$('.day_active').remove();
	 widthBand = $('.actives').width();
	 drawWithTimeout(0, session, instance, bandType);
}


function drawWithTimeout(numbSession, session, instance, bandType) {
	var TIMEOUT = 2;
	if (session[numbSession]) {
	switch(bandType) {		
	 case 'day':
		drawBlockDay(instance, findGreenZone(session[numbSession]), 'green');
		drawBlockDay(instance, findYellowZone(session[numbSession]), 'yellow');
		drawBlockDay(instance, findRedZone(session[numbSession]), 'red');
	break;
	case 'month':
		drawBlockMonth(instance, findGreenZone(session[numbSession]), 'green');
		drawBlockMonth(instance, findYellowZone(session[numbSession]), 'yellow');
		drawBlockMonth(instance, findRedZone(session[numbSession]), 'red');
	break;
	}
	
	Meteor.setTimeout(function(){drawWithTimeout(numbSession+1, session, instance, bandType)}, TIMEOUT);
	}
}