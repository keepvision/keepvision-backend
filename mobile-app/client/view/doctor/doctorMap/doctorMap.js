var statusGeolocation = new ReactiveVar(false),
		errorGeolocation = new ReactiveVar(false),
		currentCoord;

setMarkers = function(map) {
	var doctors = Meteor.users.find({roles:'doctor'}).fetch();
	doctors.forEach(function(doctor) {
		var doctorLatlng = new google.maps.LatLng(doctor.profile.latitude, doctor.profile.longitude);
		var marker = new google.maps.Marker({
			position: doctorLatlng,
			map: map.instance,
			title: doctor.profile.last_name + ' ' + doctor.profile.first_name,
			doctorId:doctor._id
		});
		google.maps.event.addListener(marker, 'click', function() {
			Router.go('doctor', {doctorId:this.doctorId});
		});
	});
	
	if (currentCoord) {
  	var latLng = new google.maps.LatLng(currentCoord.latitude, currentCoord.longitude),
		marker = new google.maps.Marker({
			position: latLng,
			map: map.instance,
			icon: '/man.png'
		});
	} 
}

Template.doctorMap.events({
	'touchstart .back':function(e) {
		e.preventDefault();
		history.back();
	},
	'touchstart #skipGeolocation':function(e) {
		e.preventDefault();
		statusGeolocation.set(true);
	},
	'touchstart #repeatGeolocation':function(e) {
		errorGeolocation.set(false);
		location.reload();
	}
});

Template.doctorMap.helpers({
  exampleMapOptions: function() {  	
    if (GoogleMaps.loaded()) {
    	if (currentCoord) {
    		var latLng = new google.maps.LatLng(currentCoord.latitude, currentCoord.longitude),
    		  zoom = 12;
    	} else {
    		var latLng = new google.maps.LatLng(-37.8136, 144.9631),
    			zoom = 2;
    	}
      return {
        center: latLng,
        zoom: zoom,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      };
    }
  },
  geolocationAvailable:function() {
  	navigator.geolocation.getCurrentPosition(
		function(position){
			statusGeolocation.set(true);
			currentCoord = position.coords;
		},
		function(err){
			errorGeolocation.set(true);
		},
	  {
			timeout:3000
		});
  	return 	statusGeolocation.get();
  },
  errorGeolocation:function() {
  	return errorGeolocation.get();
  }
});

Template.doctorMap.onCreated(function() { 

	statusGeolocation.set(false);
	errorGeolocation.set(false);
		
  GoogleMaps.ready('doctorMap', function(map) {
  	var headerHeight = $('#header').height(),
  	mapElem = document.getElementById('map-canvas'); 
	  if (mapElem) {
	  	mapElem.setAttribute( "style", "height:" + (window.innerHeight - headerHeight) + "px;" ); 
	  }
  	setMarkers(map);
  });
});