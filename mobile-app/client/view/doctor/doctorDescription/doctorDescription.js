Template.doctorDescription.helpers({
	full_name:function() { 
		if (this.profile) return this.profile.last_name + ' ' + this.profile.first_name;
	}//,
	// type_doctor:function(){
	// 	if (!this.profile) return;
	// 	if (this.profile.type_doctor === CONSTANTS.DOCTOR_PRIVATE) return 'Clinic';
	// 	if (this.profile.type_doctor === CONSTANTS.DOCTOR_CLINIC) return 'Private doctor';
	// }
});

Template.doctorDescription.events({
	'touchstart .back':function(e) {
		e.preventDefault();
		history.back();
	},
	'touchstart .view_location':function(e) {
		e.preventDefault();
		Router.go('doctorMap', {
			doctorId:this._id
		});
	},
	'touchstart .call':function(e) {
		 if (this.profile.phone) {
		 	document.location.href = 'tel:' + this.profile.phone;
		}
	}
});