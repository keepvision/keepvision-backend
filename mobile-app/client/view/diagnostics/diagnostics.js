Template.diagnostics.onRendered( function() {
  drawPieChart();
  drawGraph(this.data.diagnosisId);
});

function drawPieChart() {
    var widthPieChart =  $('#pieChartVisionSafety').width();
     $('.pieChart').easyPieChart({
       size:widthPieChart,
       lineWidth:parseInt(widthPieChart / 15, 10),
       scaleColor:false,
       barColor:'#51ace8',
       trackColor:'#e1e7e9'
    });
}

function drawGraph(diagnosisId) {

  var ctx = document.getElementById("graphVisionSafety").getContext("2d"),
  labels = [],
  data = [];

  var diagnosCenter = Collection.diagnosis.findOne(new Mongo.ObjectID(diagnosisId));
  if (diagnosCenter) {
     labels.push(moment(diagnosCenter.date_update).format('DD.MM.YYYY'));
     data.push(diagnosCenter.vision_safety);

    var diagnosLeft = Collection.diagnosis.findOne( {date_update:{'$lt':diagnosCenter.date_update}}, 
                                   {sort:{date_update:-1}} );
    if (diagnosLeft) {
      labels.push(moment(diagnosLeft.date_update).format('DD.MM.YYYY'));
      data.push(diagnosLeft.vision_safety);
    }

    var diagnosRight = Collection.diagnosis.findOne( {date_update:{'$gt':diagnosCenter.date_update}}, 
                                   {sort:{date_update:1}} );
    if (diagnosRight) {
      labels.push(moment(diagnosRight.date_update).format('DD.MM.YYYY'));
      data.push(diagnosRight.vision_safety);
    }
  }

  //Если точка одна, то добавляем фиктивную точку, для рисования прямой
  if (data.length === 1) {
    labels.push('');
     data.push(diagnosis[0].vision_safety);
  }

  var data = {
      labels: labels,
      datasets: [
          {
              fillColor: "#d0f0ff",
              strokeColor: "#59cbff",
              pointColor: "rgba(220,220,220,1)",
              pointStrokeColor: "#fff",
              pointHighlightFill: "#fff",
              pointHighlightStroke: "rgba(220,220,220,1)",
              data: data
          }
      ]
  };

  var myLineChart = new Chart(ctx).Line(data, 
    {scaleOverride: true, scaleStartValue: 0, scaleStepWidth: 20, scaleSteps: 5, 
      scaleFontSize: parseInt(screen.width / 30, 10)});
}


Template.diagnostics.helpers({
  date_measurement:function() {
    var diagnosis = Collection.diagnosis.findOne(new Mongo.ObjectID(this.diagnosisId));
    if (diagnosis) return moment(diagnosis.date_update).format('DD-MM-YYYY');
  },
  vision_safety:function() {
   var diagnosis = Collection.diagnosis.findOne(new Mongo.ObjectID(this.diagnosisId));
    if (diagnosis) return diagnosis.vision_safety.toFixed(0);
  },
  visual_acuity:function() {
    var diagnosis = Collection.diagnosis.findOne(new Mongo.ObjectID(this.diagnosisId));
    if (diagnosis && diagnosis.diagnosis_data) {
      var denominator = parseInt(20 / diagnosis.diagnosis_data.acuity, 10);
      return diagnosis.diagnosis_data.acuity + ' (20/' +  denominator +')';
    }
  },
  childName:function() {
    var diagnosis = Collection.diagnosis.findOne(new Mongo.ObjectID(this.diagnosisId));
    if (diagnosis && diagnosis.child) {
      var child = Collection.child.findOne(diagnosis.child.id);
      if (child) return child.name;
    } 
  },
  graphVisionSafetyWidth:function() {
    return screen.width * 0.8;
  },
  graphVisionSafetyHeight:function() {
    return  screen.width * 0.8 / 2;
  }
});

Template.diagnostics.events({
  'click .back':function(e) {
      e.preventDefault();
      history.back();
    }
});