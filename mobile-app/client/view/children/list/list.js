Template.listChild.helpers({
	childrenList:function() {
		return Collection.child.find({},{sort:{name:1}})
	}
})

Template.listChild.events({
	'touchstart .btn-add-child':function(e) {
		e.stopPropagation();
		Router.go('addChild');
	}
});


Template.listChild.events({
	'touchstart .child_item':function(e) { 
		beginCoordX = e.originalEvent.targetTouches[0].pageX;
		left = parseInt($('.child_item').css('left'), 10);
		widthTotal= $('.child_item').width();
	},
	'touchmove .child_item':function(e) {
		var change = e.originalEvent.targetTouches[0].pageX - beginCoordX + left,
		changePer = parseInt(change/widthTotal * 100, 10);
		if ((-32 <= changePer) && (changePer <= 0))
			e.currentTarget.style.left = change +'px';
		
	},
	'touchend .child_item':function(e){
		var leftPer = parseInt(parseInt(e.currentTarget.style.left, 10) / widthTotal * 100, 10);
		if (leftPer < -24) new_left = '-32%';
		else if (leftPer > -6) new_left = '0%'; 
		else new_left = '-14%';
		$(e.currentTarget).animate({left: new_left}, 200);
	}
});