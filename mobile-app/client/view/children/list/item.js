Template.childItem.events({
	'click .child_item':function(e) {
		e.preventDefault();
		e.stopPropagation();
		Router.go('reportUse', {
			childId:this._id, 
			date:utilLib.getDateRouter(new Date)
		});
	},
	'touchstart .edit':function(e) {
		e.preventDefault();
		e.stopPropagation();
		Router.go('editChild', {idChild:this._id});
	},
	'touchstart .remove':function(e) {
		e.preventDefault();
		e.stopPropagation();
		Meteor.call('removeChild', this._id);
	} 
});

Template.childItem.helpers({
	'imageGender':function() {
		if (this.gender === CONSTANTS.CHILD_GENDER_FEMALE) return 'femaleImage';
		else if (this.gender === CONSTANTS.CHILD_GENDER_MALE) return 'maleImage';
		
	},
	'time_session':function() {
		var usageSession = Collection.usageSession.findOne(new Mongo.ObjectID(this.last_usage_session_id));
		if (!usageSession) return;
		return moment(usageSession.date_end).startOf().fromNow();      

	},
	'status':function() {
		var TIME_OFFLINE = 5;
		var usageSession = Collection.usageSession.findOne(new Mongo.ObjectID(this.last_usage_session_id));
		if (!usageSession) return 'grey';
		var diffMinutes = moment().diff(usageSession.date_end, 'minutes');
		if (diffMinutes > TIME_OFFLINE) return 'grey';
		if (usageSession.date_red) return 'danger';
		if (usageSession.date_yellow) return 'warning';
		return 'ok';
	}
});