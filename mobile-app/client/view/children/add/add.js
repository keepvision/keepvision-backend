AutoForm.hooks({
	addChildrenForm:{
		before:{
			addChild:function(doc) {
				if (Meteor.status().connected) {
					doc.gender = +$("input[type='radio']:checked").val();
					doc.options = {};
					if ($("#deseaseInput").prop('checked')) doc.options.parents_desease_vision = CONSTANTS.CHILD_PARENTS_DESEASE_TRUE;
					else doc.options.parents_desease_vision = CONSTANTS.CHILD_PARENTS_DESEASE_FALSE;
					doc.options.sharpness = +$("#slider").val();
					return doc;
				}
				else {
					Session.set('messageServer', 'Server is unavailable');
				}
			}
		},
		after:{
			addChild:function(error) {
				if(error) alert(error);
				else Router.go('listChild');
			}	
		}
	}
});


Template.addChild.events({
	'touchstart .back':function(e) {
		e.preventDefault();
		history.back();
	},
	'touchstart label#desease':function() {
		var $deseaseInput =	$("#deseaseInput"),
		val = $deseaseInput.prop("checked");
		$deseaseInput.prop("checked", !val);
		$deseaseInput.change();
	}
});

Template.addChild.onRendered(function(){
	$(".radio_block").on("touchstart", function(){
		var self = $(this);
		self.siblings(".radio_input").prop("checked", true);
	});
	$('.male').trigger('touchstart');

	$.material.init();

	$('#birthday').datepicker({
	 	format: "dd.mm.yyyy",
    weekStart: 1,
    autoclose: true
	 });

	//отключаем клавиатуру
	 $('#birthday').focus(function() {
 		 this.blur();
		});

	$("#slider").noUiSlider({
		start: 1, 
		step:0.1,
		connect: "lower",
		range: {
			'min': [0],
			'max': [1]
		}
	});

	var $deseaseInput =	$("#deseaseInput");
	$deseaseInput.on('change', function(e) {
		$deseaseSymbol = $('#deseaseSymbol');
		if (e.currentTarget.checked) {
			$deseaseSymbol.removeClass('mdi-toggle-check-box-outline-blank');
			$deseaseSymbol.addClass('mdi-toggle-check-box');
		} else {
			$deseaseSymbol.removeClass('mdi-toggle-check-box');
			$deseaseSymbol.addClass('mdi-toggle-check-box-outline-blank');
		}
	});

	var wrapper = document.createElement('div')
	wrapper.style.cssText = "width:60px;height:60px;top:-30px;left:-30px;position:relative;";
	circle = $(".noUi-handle-lower")
	circle.append(wrapper);

	Session.set('sharpness', $("#slider").val());

	$("#slider").on({
		slide: function(e){
			Session.set('sharpness', $("#slider").val());
		}
	});
});

Template.addChild.helpers({
	sharpness:function() { 
		return	Session.get('sharpness');
	},
	messageServer:function() {
		return Session.get('messageServer');
	}
});

