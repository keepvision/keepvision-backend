AutoForm.hooks({
	editChildrenForm:{
		before:{
			editChild:function(doc) {
				if (Meteor.status().connected) {
					doc.gender = +$("input[type='radio']:checked").val();
					doc.options = {};
					if ($("#deseaseInput").prop('checked')) doc.options.parents_desease_vision = CONSTANTS.CHILD_PARENTS_DESEASE_TRUE;
					else doc.options.parents_desease_vision = CONSTANTS.CHILD_PARENTS_DESEASE_FALSE;
					doc.options.sharpness = +$("#slider").val();
					return doc;
				}
				else {
					Session.set('messageServer', 'Server is unavailable');
				}
			}
		},
		after:{  
			editChild:function(error) {
				if(error) alert(error);
				else history.back();
			}	
		},
		docToForm:function(doc, ss) {
			doc.birthday = moment(doc.birthday).format('DD.MM.YYYY');
			return doc;
		}
	}
});

Template.editChild.onRendered(function() {
	var dataTemplate = Template.currentData();
	$.material.init();

	 $('#birthday').datepicker({
	 	format: "dd.mm.yyyy",
    weekStart: 1,
    language: "ru",
    autoclose: true

	 });

	 $('#birthday').focus(function() {
 		 this.blur();
		});

	$("#slider").noUiSlider({
		start: 1, 
		step:0.1,
		connect: "lower",
		range: {
			'min': [0],
			'max': [1]
		}
	});

	var wrapper = document.createElement('div')
	wrapper.style.cssText = "width:60px;height:60px;top:-30px;left:-30px;position:relative;";
	circle = $(".noUi-handle-lower")
	circle.append(wrapper);


	$("#slider").on({
		slide: function(e){
			Session.set('sharpness', (+$("#slider").val()).toFixed(1));
		}
	});


$(".radio_block").on("click", function(){
		var self = $(this);
		self.siblings(".radio_input").prop("checked", true);
	});

	var $deseaseInput =	$("#deseaseInput");
		$deseaseInput.on('change', function(e) {
			$deseaseSymbol = $('#deseaseSymbol');
			if (e.currentTarget.checked) {
				$deseaseSymbol.removeClass('mdi-toggle-check-box-outline-blank');
				$deseaseSymbol.addClass('mdi-toggle-check-box');
			} else {
				$deseaseSymbol.removeClass('mdi-toggle-check-box');
				$deseaseSymbol.addClass('mdi-toggle-check-box-outline-blank');
			}
		});
 	

	
	if (dataTemplate) {
		if (dataTemplate.gender === CONSTANTS.CHILD_GENDER_MALE) $('.male').click();
		else if (dataTemplate.gender === CONSTANTS.CHILD_GENDER_FEMALE) $('.female').click();
		if (dataTemplate.options && dataTemplate.options.parents_desease_vision &&	
			dataTemplate.options.parents_desease_vision === CONSTANTS.CHILD_PARENTS_DESEASE_TRUE ) {
				var $deseaseSymbol = $('#deseaseSymbol');
				$deseaseSymbol.removeClass('mdi-toggle-check-box-outline-blank');
				$deseaseSymbol.addClass('mdi-toggle-check-box');
				$deseaseInput =	$("#deseaseInput");
				$deseaseInput.prop('checked', true);
		}
			if (dataTemplate.options.sharpness) {
				$("#slider").val(dataTemplate.options.sharpness);
				Session.set('sharpness', dataTemplate.options.sharpness);
			}
		}
	});

Template.editChild.events({
	'touchstart .back':function(e) {
		e.preventDefault();
		history.back();
	},
	'touchstart label#desease':function() {
		var $deseaseInput =	$("#deseaseInput"),
		val = $deseaseInput.prop("checked");
		$deseaseInput.prop("checked", !val);
		$deseaseInput.change();
	}
});

Template.editChild.helpers({
	sharpness:function() { 
		return	Session.get('sharpness');
	},
	messageServer:function() {
		return Session.get('messageServer');
	}
});