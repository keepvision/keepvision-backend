Template.reportUseDay.onRendered(function() {
  var self = this;
  self.autorun(function () {
    var dataContext = Template.currentData(),
				usageSessionSign = self.subscribe("usageSessionDay", viewScreenUseDay.childId, dataContext.date),
     		diagnosisSign = self.subscribe("diagnosisByDay", viewScreenUseDay.childId, dataContext.date),
      	instance = Template.instance(),
      	date = instance.data.date,
      	dateBegin = new Date(date);
				dateBegin.setHours(8);	
				var dateEnd = utilLib.nextDay(dateBegin);

       if (diagnosisSign.ready()) {
      	var diagnosis = Collection.diagnosis.find({date_update:{ 
					"$gte": dateBegin,
					"$lt": dateEnd
				}}).fetch();
				  symbolDiagnosis.draw(instance, diagnosis, 'day');
      }
      	 
     	if (usageSessionSign.ready()) {;
      	var session = Collection.usageSession.find({date_begin:{ 
					"$gte": dateBegin,
					"$lt": dateEnd
				}}).fetch();
      	 bandTime.draw(instance, session, 'day');
      }
  });
});


Template.reportUseDay.helpers({
	dateHours:function() {
		if (this.date) return moment(this.date).format('LL');
	},
	gridHours:function() {
		var HOUR_IN_DAY = 24;
		grid = [];
		for (var i = 8; i <= 31; i++) {
			grid.push({
				timeStr:((i<HOUR_IN_DAY)?i:i-HOUR_IN_DAY) + ':00',
				hour:i
			});
		}
		return grid;
	}
});

Template.reportUseDay.events({
	'click .eye':function(e) {
		e.preventDefault();
		if (e.currentTarget.id) Router.go('diagnostics', {
			diagnosisId:e.currentTarget.id
		});
	}
});
