var activeTab = 'day';

Template.reportUse.helpers({
  'children_name':function() {
    var child = Collection.child.findOne(this.childId);
    if (child) return child.name;
  }
});

Template.reportUse.events({
 "touchstart #day_tab_head":function() {
   activeTab = 'day';
 },
 "touchstart #month_tab_head":function() {
   activeTab = 'month';
   var $innerMonth = $('.month > .center-screen');
  if ($innerMonth) $('#tab_month').height($innerMonth.height());

 },
 'touchstart .back':function(e) {
  e.preventDefault();
  Router.go('/');
},
'touchstart .center-screen':function(e) { 
  var self = $(e.currentTarget)[0];
  var leftScreen = self.previousSibling;
  var rightScreen = self.nextSibling;
  beginCoordX = e.originalEvent.targetTouches[0].pageX;
  beginCoordXLeft = +getTransform(leftScreen)[0];
  beginCoordXRight = +getTransform(rightScreen)[0];
},
'touchmove .center-screen':function(e) {
  var self = $(e.currentTarget)[0];
  var change = e.originalEvent.targetTouches[0].pageX - beginCoordX;
  self.style.transform = 'translate3d(' + change + 'px, 0, 0)';
  self.style.webkitTransform = 'translate3d(' + change + 'px, 0, 0)';
  self.classList.remove('animate-screen');

  var leftScreen = self.previousSibling,
  changeLeft =  beginCoordXLeft + change;
  leftScreen.style.transform = 'translate3d(' + changeLeft + 'px, 0, 0)';
  leftScreen.style.webkitTransform = 'translate3d(' + changeLeft + 'px, 0, 0)';
  leftScreen.classList.remove('animate-screen');

  var rightScreen = self.nextSibling,
  changeRight =  beginCoordXRight + change;
  rightScreen.style.transform = 'translate3d(' + changeRight + 'px, 0, 0)';
  rightScreen.style.webkitTransform = 'translate3d(' + changeRight + 'px, 0, 0)';
  rightScreen.classList.remove('animate-screen');

},
'touchend .center-screen':function(e) {
   if ( activeTab === 'day') {
    var parent = document.getElementById('tab_day');
    var viewScreenUse = viewScreenUseDay;
  }
  else {
    var parent = document.getElementById('tab_month');
    var viewScreenUse = viewScreenUseMonth;
  }

  var self = $(e.currentTarget)[0],
  leftScreen = self.previousSibling,
  rightScreen = self.nextSibling;
  currentScroll = +getTransform(self)[0]
  var width = $('.inner').width();

  self.style.transform = '';
  self.style.webkitTransform = '';
  self.classList.add('animate-screen');

  leftScreen.style.transform = '';
  leftScreen.style.webkitTransform = '';
  leftScreen.classList.add('animate-screen');

  rightScreen.style.transform = '';
  rightScreen.style.webkitTransform = '';
  rightScreen.classList.add('animate-screen');
  if (currentScroll >= width / 2 ) {
    scrollScreens.scrollRight(viewScreenUse, parent);
  }
  else if ((currentScroll < 0) && (Math.abs(currentScroll) >=   width / 2 )) {
    scrollScreens.scrollLeft(viewScreenUse, parent);
  }
},
'touchstart .prev':function(){
  if ( activeTab === 'day') {
    var parent = document.getElementById('tab_day');
    var viewScreenUse = viewScreenUseDay;
  }
  else {
    var parent = document.getElementById('tab_month');
    var viewScreenUse = viewScreenUseMonth;
  }

   scrollScreens.scrollRight(viewScreenUse, parent);
   var $innerMonth = $('.month > .center-screen');
  if ($innerMonth) $('#tab_month').height($innerMonth.height());
},
'touchstart .next':function(e) {
  if ( activeTab === 'day') {
    var parent = document.getElementById('tab_day');
    var viewScreenUse = viewScreenUseDay;
  }
  else {
    var parent = document.getElementById('tab_month');
    var viewScreenUse = viewScreenUseMonth;
  }
  scrollScreens.scrollLeft(viewScreenUse, parent);
  var $innerMonth = $('.month > .center-screen');
  if ($innerMonth) $('#tab_month').height($innerMonth.height());
}
});

Template.reportUse.onRendered( function() {
   $('body').hammer({
    drag_min_distance:1,
    swipe_velocity:0.1
  });

  scrollScreens.initialScreens(
    this.data, 
    Template.reportUseDay, 
    document.getElementById('tab_day'), 
    viewScreenUseDay, 
    'day');
  scrollScreens.initialScreens(
    this.data, 
    Template.reportUseMonth, 
    document.getElementById('tab_month'), 
    viewScreenUseMonth, 
    'month');

  var $tabs = $("#tabs"),
  $header = $("#header");

  $.material.init();

  $tabs.find(".tab").on("touchstart", function(){
    var $tab = $(this),
    screen = $tab.data("target");

    if(!$tab.hasClass("active")){
      $tab.siblings(".active").removeClass("active");
      $tab.addClass("active");
    }  

    $(".tab_panel:visible").fadeOut();
    $(".tab_panel." + screen).fadeIn();
  });

     // Высоту вкладок задаем скриптом т.к. все блоки внутри имееют абсолютное позиционирование
  var $innerDay = $('.day > .inner');
  if ($innerDay)  $('#tab_day').height($innerDay.height());



});

function getTransform(el) {
    var results = $(el).css('-webkit-transform').match(/matrix(?:(3d)\(-{0,1}\d+(?:, -{0,1}\d+)*(?:, (-{0,1}\d+))(?:, (-{0,1}\d+))(?:, (-{0,1}\d+)), -{0,1}\d+\)|\(-{0,1}\d+(?:, -{0,1}\d+)*(?:, (-{0,1}\d+))(?:, (-{0,1}\d+))\))/);
    if(!results) return [0, 0, 0];
    if(results[1] == '3d') return results.slice(2,5);

    results.push(0);
    return results.slice(5, 8);
}