Template.reportUseMonth.onRendered(function() {
// 	$('body').hammer({
//     drag_min_distance:1,
//     swipe_velocity:0.1
//   });
  var self = this;
  self.autorun(function () {
     var dataContext = Template.currentData();
     var usageSessionSign = self.subscribe("usageSessionMonth", viewScreenUseMonth.childId, dataContext.date);
     var diagnosisSign = self.subscribe("diagnosisByMonth", viewScreenUseMonth.childId, dataContext.date);
     

     var instance = Template.instance(),
      	date = instance.data.date,
   			year = date.getFullYear(),
				month = date.getMonth(),
				beginMonth = new Date(year, month, 1),
				endMonth = new Date(year, +month+1, 1);


      if (diagnosisSign.ready()) {
      	var diagnosis = Collection.diagnosis.find({date_update:{ 
					"$gte": beginMonth,
					"$lt": endMonth
				}}).fetch();
				  symbolDiagnosis.draw(instance, diagnosis, 'month');
      }

      if (usageSessionSign.ready()) {
      // $.snackbar({content: "This is my awesome snackbar!", timeout:0}); 
      	var session = Collection.usageSession.find({date_begin:{ 
					"$gte": beginMonth,
					"$lt": endMonth
				}}).fetch();
      	 bandTime.draw(instance, session, 'month');
      }
  });
});


Template.reportUseMonth.helpers({
	name:function() {
		return this.name;
	},
	dateMonth:function() {
		if (this.date) return moment(this.date).format('MMM, YYYY');
	},
	gridMonth:function() {
		grid = [];
		for (var i = 1; i <= utilLib.getNumbDayMonth(this.date); i++) {
			grid.push({
				day:i
			});
		}
		return grid;
	}
});

Template.reportUseMonth.events({
	'click .eye':function(e) {
		e.preventDefault();
		e.stopPropagation();
		if (e.currentTarget.id) Router.go('diagnostics', {
			diagnosisId:e.currentTarget.id
		});
	}//,
	// 'click .day_hour':function(e, template) { 
	// 	e.stopPropagation();
	// 	var classArr = e.currentTarget.className.split(' ');
	// 	if (!classArr.length) return;
	// 	var newDate = template.data.date;
	// 	newDate.setDate(classArr[0]);
	// 	Router.go('reportUse',{
	// 		childId:viewScreenUseMonth.childId,
	// 		date:utilLib.getDateRouter(newDate)
	// 	});
	// }
});

