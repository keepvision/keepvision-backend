this.Collection = this.Collection || {};
this.Schems = this.Schems || {};

Schems.userDataForChild = new SimpleSchema({
	id:{
		type:String
	}
});

Schems.optionsChild = new SimpleSchema({
	parents_desease_vision:{
	 	type: Number,
	 	optional:true
   },
   sharpness:{
   	type:Number,
   	decimal:true,
   	optional:true
   }
});

Schems.childServer = new SimpleSchema({
	user:{
		type:Schems.userDataForChild,
		optional:true
	},
	name:{
		type:String,
		min:1
	},
	gender:{ 
		type:Number,

		autoform: {
			type:"select-radio",
			options:function() {
				return [
					{ label:"male", value: CONSTANTS.CHILD_GENDER_MALE},
					{ label:"female", value: CONSTANTS.CHILD_GENDER_FEMALE}
				]
			}
		}
	},
	birthday:{
		type:Date,
		custom:function() {
			var ageDifMs = Date.now() - this.value;
			var ageDate = new Date(ageDifMs);
			ageDate.getUTCFullYear();
			var age = Math.abs(ageDate.getUTCFullYear() - 1970);
			if (age >= 18) return 'AgeGreater18';
		}
	},
	date_update:{
		type:Date,
		optional:true
	},
	status:{
		type:Number,
		optional:true
	},
	options:{
		type:Schems.optionsChild
	}
});

Collection.child = new Mongo.Collection('child'); 
Collection.child.attachSchema(Schems.childServer);

Schems.childClient= new SimpleSchema({
	name:{
		type:String,
		min:1
	},
	gender:{ 
		type:Number
	},
	birthday:{
		type:String,
		custom:function() {
			var date = utilLib.strToDate(this.value);
			if (!date) return "it'sNotDate";
			var ageDifMs = Date.now() - date;
			var ageDate = new Date(ageDifMs);
			ageDate.getUTCFullYear();
			var age = Math.abs(ageDate.getUTCFullYear() - 1970);
			if (age >= 18) return 'AgeGreater18';
		}
	},
	options:{
		type:Schems.optionsChild
	}
	 
});