this.Scheme = this.Scheme || {};
Scheme.forgotPassword = new SimpleSchema({
	email:{
		type:String,
		regEx: SimpleSchema.RegEx.Email
	}
});