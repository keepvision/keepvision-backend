Scheme.changePassword = new SimpleSchema({
	passwordOld: {
		type:String,
		min:6
	},
	password: {
		type:String,
		min:6
	},
	passwordCopy: {
		type:String,
		min:6,
		custom: function () {
      if (this.value !== this.field('password').value) {
        return "passwordMismatch";
      }
    }
	}
});