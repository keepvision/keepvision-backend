this.Scheme = this.Scheme || {};
Scheme.authorization = new SimpleSchema({
	email:{
		type:String,
		regEx: SimpleSchema.RegEx.Email
	},
	password:{
		type:String,
		min:6
	}
});