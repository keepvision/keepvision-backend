this.Scheme = this.Scheme || {};
Scheme.registrtion = new SimpleSchema({
	first_name: {
		type:String,
		min:1
	},
	last_name: {
		type:String,
		min:1
	},
	email:{
		type:String,
		regEx: SimpleSchema.RegEx.Email
	},
	password:{
		type:String,
		min:6
	}
});