Router.configure({
	layoutTemplate:'layout',
	loadingTemplate:'loading',
	waitOn: function() { if (Meteor.status().connected) return Meteor.subscribe('children'); }
});


Router.onBeforeAction('loading');

Router.onBeforeAction(function(){
	Session.set('messageServer','');
	this.next();
});

Router.onBeforeAction(function() {
	if (!Meteor.userId()) {
		var email = window.localStorage.getItem('email'),
		password = window.localStorage.getItem('password');
		if (!email || !password) Router.go('authorization');
		else {
			Meteor.loginWithPassword(email, password, function(err) {
				if (err) Router.go('authorization');
			});
		}
		this.next();
	}
	else this.next();
},
{except: ['authorization', 'forgotPassword', 'registration', 'userAgreement']});


Router.onBeforeAction(function() {
  GoogleMaps.load();
  this.next();
}, { only: ['doctorMap'] });