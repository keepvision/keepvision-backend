Router.route('diagnostics', {
	path:'/diagnostics/diagnosisId/:diagnosisId',
	waitOn:function(){
		return [
			Meteor.subscribe('diagnosisByIdLeft', this.params.diagnosisId),
			Meteor.subscribe('diagnosisById', this.params.diagnosisId),
			Meteor.subscribe('diagnosisByIdRight', this.params.diagnosisId)
		]
	},
	data:function() {
		return {diagnosisId:this.params.diagnosisId};
	}
});