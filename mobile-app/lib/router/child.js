Router.route('listChild', {
	path:'/',
	waitOn:function() {
		var children = Collection.child.find().fetch();
		children.forEach(function(child){
			Meteor.subscribe('lastUsageSessionChild', child._id);
		});
		
	}
});

Router.route('addChild', {
	path:'/addChild'
});

Router.route('editChild', {
	path:'editChild/idChild/:idChild',
	// waitOn:function() {
	// 	Meteor.subscribe('oneChild', this.params.idChild);
	// },
	data:function() {
		return Collection.child.findOne(this.params.idChild);		
	}
})