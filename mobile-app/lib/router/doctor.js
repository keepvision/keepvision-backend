Router.route('doctorMap', {
	path:'/doctorMap/doctorId/:doctorId?',
	waitOn:function() {
		if (this.params.doctorId) Meteor.subscribe('doctor', this.params.doctorId);
		else return Meteor.subscribe('doctors');
	}
});

Router.route('doctor', {
	path:'/doctor/doctorId/:doctorId',
	template:'doctorDescription',
	waitOn:function() {
		return Meteor.subscribe('doctor', this.params.doctorId);
	},
	data:function() {
		return Meteor.users.findOne(this.params.doctorId);
	}
});