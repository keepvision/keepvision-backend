Router.route('reportUse', {
	path:'reportUse/childId/:childId/date/:date',
	// waitOn:function() {
	// 	return [Meteor.subscribe("usageSessionDay", this.params.childId, new Date(this.params.date)),
	// 	 				Meteor.subscribe("diagnosisByDay", this.params.childId, new Date(this.params.date))];
	// },
	data:function() {
		return {
			childId: this.params.childId,
			date: this.params.date
		}
	}
});

// Router.route('reportUseMonth', {
// 	path:'reportUseMonth/childId/:childId/date/:date/typeReport/:typeReport',
// 	template:'reportUse',
// 	data:function() {
// 		return {
// 			childId: this.params.childId,
// 			date: this.params.date,
// 			typeReport: this.params.typeReport	
// 		}
// 	}
// });


