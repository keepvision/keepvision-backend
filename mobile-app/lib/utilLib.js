utilLib = {};
utilLib.getDateRouter = function(date) {
	if (!date) return;
	return date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
}

//Получить следующий день
utilLib.nextDay = function(date) {
 if (!date) return;
 var day = date.getDate(),
 tomorrow = new Date(date);
 tomorrow.setDate( day + 1 );
 return tomorrow;
}

//Получить предыдущий день
utilLib.prevDay = function(date) {
  if (!date) return;
  var day = date.getDate(),
  yesterday = new Date(date);
  yesterday.setDate( day - 1 );
  return yesterday;
}

//Получить следующий месяц
utilLib.nextMonth = function(date) {
	if (!date) return;
	var nextMonth = date.getMonth();
	return new Date(date.getFullYear(), ++nextMonth, date.getDate());
}

//Получить предыдущий месяц
utilLib.prevMonth = function(date) {
	if (!date) return;
	var nextMonth = date.getMonth();
	return new Date(date.getFullYear(), --nextMonth, date.getDate());
}


//Получить количество дней в месяце
utilLib.getNumbDayMonth = function(date) {
	if (!date) return;
	var month = date.getMonth();
	month++;
	var lastDayMonth = new Date(date.getFullYear(),month,0);
	return lastDayMonth.getDate();
}

// Переводит строку с датой формата DD.MM.YYYY в Date
utilLib.strToDate = function(dateStr) {
	if (!dateStr) return;
	var compDate = dateStr.split('.'),
	parse =	Date.parse(compDate[1]+'.'+compDate[0]+'.'+compDate[2]);
	if (isNaN(parse)) return;
	return new Date(parse);
}

// Ребенок этого родителя
utilLib.childThisParent = function(childId, parentId) {
	if (!(childId && parentId )) return [];
	var child = Collection.child.findOne(childId);
	if (!child) return false;
	if (child.user.id === parentId) return true;
	return false;
}