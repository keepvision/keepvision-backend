Meteor.methods({
	resetPasswordUser:function(email) {
		var LENGTH_PASSWORD = 8;
		var user = Meteor.users.findOne({'emails.address':email},{fields:{_id:1}});
		if (!user) throw new Meteor.Error('User not found', 'User not found');
		var	newPassword = Random.id([LENGTH_PASSWORD]);
		Accounts.setPassword(user._id, newPassword);
		Email.send({
      to: email,
      from: 'keep@vision.com',
      subject: 'keep vision reset password',
      text: 'Your new password: ' + newPassword
    });
	}
});