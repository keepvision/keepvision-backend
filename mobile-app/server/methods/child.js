Meteor.methods({
	addChild:function(doc) {
		var userId = Meteor.userId();
		if (!userId) throw new Meteor.Error('Please log in.');
		doc.date_update = new Date();
		doc.user = {};
		doc.user.id = userId;
		doc.status = CONSTANTS.CHILD_STATUS_ACTIVE;
		doc.birthday = utilLib.strToDate(doc.birthday);
		Collection.child.insert(doc)
	},
	editChild:function(doc, upd, childId) {
		var userId = Meteor.userId();
		if (!userId) throw new Meteor.Error('Please log in.');
		doc.date_update = new Date();
		doc.birthday = utilLib.strToDate(doc.birthday);
		Collection.child.update({_id:childId,'user.id':userId}, {$set:doc});
	},
	removeChild:function(childId) {
		var userId = Meteor.userId();
		if (!userId) throw new Meteor.Error('Please log in.');
		Collection.child.update({_id:childId,'user.id':userId}, {$set:{status:CONSTANTS.CHILD_STATUS_REMOVE}});
	}
});