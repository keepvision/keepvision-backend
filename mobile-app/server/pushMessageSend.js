Meteor.startup(function () {
	  sendMessage();
    // Push.send({
    //     from: 'keepvision',
    //     title: 'title',
    //     text: 'textTest',
    //     query: {
    //         userId: 'DCpSsjnv5C23NvXG9'
    //     },
    //     payload:{
    //       test:'test'
    //     }
    //   });
    // console.log('message is send');
});

var DELAY = 60000;
var PUSH_MESSAGE_CREATE = 0;
var PUSH_MESSAGE_SEND = 1;
function sendMessage() { 
	var startTime = new Date();
  // console.log('Send push message:' + startTime);
  Meteor.setTimeout(sendMessage, calculateDelay());
  var pushMessage = Collection.pushMessage.find({status:PUSH_MESSAGE_CREATE}).fetch();
  pushMessage.forEach( function(message) {
  	 Push.send({
        from: 'keepvision',
        title: message.title,
        text: message.text,
        query: {
            userId: message.user.id
        },
        payload:{
          childId:message.childId,
          date:message.date
        }
      });
  	 Collection.pushMessage.update(message._id, {$set:{status:PUSH_MESSAGE_SEND}});
  });	
  function calculateDelay() {
    var delay = DELAY - (new Date() - startTime);
    return (delay < 0) ? 0 : delay;
  }
}

	// PUSH_MESSAGE_CREATE:0,
	// PUSH_MESSAGE_SEND:1