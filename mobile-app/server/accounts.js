Accounts.onCreateUser(function(options, user) {
	// console.log(options, user);
	var LENGTH_API_KEY = 20;
	user.roles = ['user'];
	user.status = CONSTANTS.USER_STATUS_ACTIVE;
	user.profile = options.profile; 
	user.api_key = Random.id([LENGTH_API_KEY]);
	user.date_update = new Date;
	return user;
});

Accounts.validateLoginAttempt(function(par) { 
	if (par.error) {
		throw new Meteor.Error(403, par.error.reason, par.error.reason );
	}
	if (!(Roles.userIsInRole(par.user, 'user'))) {
		throw new Meteor.Error(403, "No user rights", "No user rights");
	}
	if (par.user.status !== CONSTANTS.USER_STATUS_ACTIVE ) {
		throw new Meteor.Error(403, "The account is not activated", "The account is not activated");
	}
	return true;
});