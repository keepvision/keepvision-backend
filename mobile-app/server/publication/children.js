Meteor.publish('children', function() {
	if (!this.userId) return [];
	return Collection.child.find({'user.id':this.userId, status:CONSTANTS.CHILD_STATUS_ACTIVE});
});

Meteor.publish('oneChild', function(childId) {
	if (!this.userId) return [];
	return Collection.child.find({_id:childId, 'user.id':this.userId });
});