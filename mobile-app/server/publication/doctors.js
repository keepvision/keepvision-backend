Meteor.publish('doctors', function(){
  if (!this.userId) return [];
  return Meteor.users.find({roles:'doctor', 
                            status:CONSTANTS.DOCTOR_STATUS_ACTIVE}, {fields:{profile:1, roles:1, emails:1}});
});

Meteor.publish('doctor', function(doctorId){
  if (!this.userId) return [];
  return Meteor.users.find({_id:doctorId,
  													roles:'doctor', 
                            status:CONSTANTS.DOCTOR_STATUS_ACTIVE}, {fields:{profile:1, roles:1, emails:1}});
});