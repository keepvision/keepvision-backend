Meteor.publish('diagnosisById', function(diagnosisId) {
	if (!this.userId) return [];
	if (!diagnosisId) return [];
	var diagnosis = Collection.diagnosis.findOne(new Mongo.ObjectID(diagnosisId));
	if (!utilLib.childThisParent(diagnosis.child.id, this.userId)) return [];
	return Collection.diagnosis.find(new Mongo.ObjectID(diagnosisId));

});

Meteor.publish('diagnosisByIdLeft', function(diagnosisId) {
	if (!this.userId) return [];
	var diagnosis =	Collection.diagnosis.findOne(new Mongo.ObjectID(diagnosisId));
	if (!diagnosis) return [];
	if (!utilLib.childThisParent(diagnosis.child.id, this.userId)) return [];

	return Collection.diagnosis.find(
	{date_update:{'$lt':diagnosis.date_update},
	 "child.id":diagnosis.child.id }, 
	{sort:{date_update:-1}, limit:1});
});

Meteor.publish('diagnosisByIdRight', function(diagnosisId) {
	if (!this.userId) return [];
	var diagnosis =	Collection.diagnosis.findOne(new Mongo.ObjectID(diagnosisId));
	if (!diagnosis) return [];
	if (!utilLib.childThisParent(diagnosis.child.id, this.userId)) return [];

	return Collection.diagnosis.find(
	{date_update:{'$gt':diagnosis.date_update},
	"child.id":diagnosis.child.id}, 
	{sort:{date_update:1}, limit:1});
});


Meteor.publish('diagnosisByDay', function(childId, day) {
	if (!this.userId) return [];
	if (!utilLib.childThisParent(childId, this.userId)) return [];
	var date = new Date(day),
	year = date.getFullYear(),
	month = date.getMonth(),
	day = date.getDate(),
	dateBegin = new Date(year, month, day, 8),
	dateEnd = new Date(year, month, (day + 1), 8);
	return Collection.diagnosis.find({date_update:{ 
			"$gte": dateBegin,
			"$lt": dateEnd
		},
		"child.id":childId
	}, {fields: {vision_safety: 1, child: 1, date_update:1}});
});

Meteor.publish('diagnosisByMonth', function(childId, date){
	if (!this.userId) return [];
	if (!(childId && date)) return [];
	if (!utilLib.childThisParent(childId, this.userId)) return [];
	var year = date.getFullYear(),
	month = date.getMonth(),
	beginMonth = new Date(year, month, 1),
	endMonth = new Date(year, +month+1, 1);
	
	return Collection.diagnosis.find({date_update:{ 
			"$gte": beginMonth,
			"$lt": endMonth
		},
		"child.id":childId
	}, {fields: {vision_safety: 1, child: 1, date_update:1}});
});