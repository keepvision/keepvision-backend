Meteor.publish('usageSessionDay', function(childId, day) {
	if (!utilLib.childThisParent(childId, this.userId)) return [];
	var date = new Date(day),
	year = date.getFullYear(),
	month = date.getMonth(),
	day = date.getDate(),
	dateBegin = new Date(year, month, day, 8),
	dateEnd = new Date(year, month, (day + 1), 8);
	return Collection.usageSession.find({date_begin:{ 
			"$gte": dateBegin,
			"$lt": dateEnd
		},
		"child.id":childId
	},{fields: {log: 0}});
});

Meteor.publish('usageSessionMonth', function(childId, date){
	if (!date) return [];
	if (!utilLib.childThisParent(childId, this.userId)) return [];
	var year = date.getFullYear(),
	month = date.getMonth(),
	beginMonth = new Date(year, month, 1),
	endMonth = new Date(year, +month+1, 1);
	return Collection.usageSession.find({date_begin:{ 
			"$gte": beginMonth,
			"$lt": endMonth
		},
		"child.id":childId
	},{fields: {log: 0}});
});


Meteor.publish('lastUsageSessionChild', function(childId) {
	if (!utilLib.childThisParent(childId, this.userId)) return [];
	var child = Collection.child.findOne(childId);
	if (!(child && child.last_usage_session_id)) return [];
	return Collection.usageSession.find(new Mongo.ObjectID(child.last_usage_session_id),{fields: {log: 0}});
});

// Meteor.publish('allUsageSession', function(childId) {
// 	return Collection.usageSession.find({
// 		"child.id":childId
// 	});
// });